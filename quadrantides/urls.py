#-*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'quadrantides.views.home', name='home'),
     url(r'^forum/', include('forum.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^deconnexion/$', 'django.contrib.auth.views.logout', {'template_name' : 'registration/logout.html', 'extra_context': {'error_context':{'success':True}}}),
    url(r'^connexion/$', 'django.contrib.auth.views.login', {'template_name': 'registration/login.html', 'extra_context': {'error_context':{'success':True}}}),
    url(r'^connexion/espace/personnel/$', 'django.contrib.auth.views.login',{'template_name': 'registration/personal_space_login.html', 'extra_context': {'error_context':{'success':True}}})
)
