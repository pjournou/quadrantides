# -*- coding: utf-8 -*-
"""
Created on Fri Oct 31 17:18:29 2014

@author: Patrice
"""
import forum.models as model

from string import maketrans
import logging



#def test_iter():
#    a = iter([PublishedReflexion(), PublishedReflexion(), PublishedReflexion()])
#    a.next()
#    a.next()
        
class PostContext(object):
    
    def __init__(self, post, sources_count, responses_count):
        self.post = post
        self.sources_count=sources_count
        self.responses_count=responses_count
#        allcontents = Discussion.objects.filter(publication = post.id)
        
        rcinfos, discussions = model.accessor_get_discussions_for_requested_publication(post)

        threads_list = []
        
        if rcinfos.success:
            for discussion in discussions:
                threads_list.append(discussion.thread)
        
        self.inthreads = threads_list
            
    def get_unique_title_for_inthreads(self): 
        title = ''
        for index in range(len(self.inthreads)):
            if index < len(self.inthreads)-1:
                title += self.inthreads[index].get_title() + ', '
            else:
                title += self.inthreads[index].get_title()  
        logging.info('get_unique_title_for_inthreads/Les fils de discussion sont : %s' % title)
        return title 

#class ListManager(object):
#    
#    def __init__(self, post, managed_list):
#        self.managed_list = managed_list
#        self.current_index=0
#        
#    def get_next_item(self):
#        if  self.current_index + 1 > len(self.managed_list):
#            # le dernier élément a déjà été atteint
#            success = False
#            return success
#        else:
#            self.current_index += 1
#            success = True
#            return success, self.managed_list[self.current_index]
#        
#    def get_item(self):
#        return self.managed_list[self.current_index]
            
        
def count_responses(post):
    linked_posts = post.linked_posts

    cnt = 0
    if linked_posts==None:
        pass
    else:
        queryset = linked_posts.get_queryset()
        logging.debug(type(queryset))
        for source in queryset:
            if source.date > post.date:
                cnt += 1
    return cnt
    
def count_sources(post):
    linked_posts = post.linked_posts
    cnt = 0
    if linked_posts==None:
        pass
    else:
        logging.info('count_sources/Les sources associées à la publication courante : %s' % type(linked_posts))

        queryset = linked_posts.get_queryset()
        logging.info('count_sources/Résultat queryset : %s' % type(queryset))
        for source in queryset:
            logging.info('count_sources/Les sources associées à la publication courante : %s' % type(source))
            if source.date < post.date:
                cnt += 1

    return cnt
    
def get_post_context(post):
    return PostContext(post, count_sources(post), count_responses(post))
    
def get_post_context_list(posts):
    data = []
    for post in posts:    
        logging.info('get_post_content_list/La publication courante est : %s' % type(post))
        data.append(get_post_context(post))
    return data


#def get_all_profiles_in_opened_thread(thread_id):
#    
#    allcontents = Discussion.objects.filter(thread=thread_id).order_by('publication_order')
#    logging.info('get_all_participants_in_opened_thread/Nombre de publication dans le fil {0} : {1}'.format(thread_id, len(allcontents)))
#
#    usernames = []
#    profiles = []
#    
#    for content in allcontents:
#        first_reflexion = content.publication.get_first_reflexion()
#        postusername = first_reflexion.profile.user.username.encode('utf8', 'replace')
#        logging.info('get_all_participants_in_opened_thread/cuurent username is %s ' % postusername)
#        if postusername not in usernames:
#            usernames.append(postusername)
#            profiles.append(first_reflexion.profile)
#        
#    return usernames, profiles
    
 

    

def has_responses(post):
    count = count_responses(post)
    return count > 0
    
def has_source(post):
    count = count_sources(post)
    return count > 0
    
def get_thread_posts_list_without_any_response(thread_id):
    
    """
        Cette fonction permet d'extraire la liste des publications d'un fil de discussion
        qui ne possèdent encore aucune réponse.
        Elles sont intéressantes car elles constituent les extrémités des fils de discussion
        thread_id = id du fil
    
    """
#    allcontents = Discussion.objects.filter(thread = thread_id)
    allcontents = model.accessor_get_discussions_from_opened_thread(thread_id)
    posts_list = []
    for content in allcontents:  
        if not(has_responses(content.publication)):
            posts_list.append(content.publication)
    return posts_list
        
#def generateur(posts):
#    for post in posts:
#        yield post
        
#def count_post_branchs(post):
#    logging.info('ENTRÉE dans la fonction count_post_branchs')
#    cnt = 0
#    linked_posts = post.linked_posts
#    
#    current_post_has_source = has_source(post)
#    list_managers = []
#    current_level = -1
#    cnt_found = False
#    
#    logging.info('count_post_branchs/La publication a-t-elle des sources ? : %s' % current_post_has_source)
#    
#    if current_post_has_source:
#        
#        queryset = linked_posts.get_queryset()
#        
#        while not(cnt_found):
#
#          # on monte d'un niveau tant que le post courant a des sources
#            
#            while current_post_has_source:
#                current_level += 1
#
#                list_managers.append(iter(queryset))
#                current_iter = list_managers[current_level]
#                current_post = current_iter.next()
#                logging.info('count_post_branchs/La publication courante dans le while est : %s' % type(current_post))
#
#                current_post_has_source = has_source(current_post)
#                if current_post_has_source:
#                    linked_posts = current_post.linked_posts
#                    queryset = linked_posts.get_queryset()
#                    
#                    
#                
#          # rupture : le niveau de la 1ère publication a été atteint
#          # un nouveau fil vient d'être caractérisé
#                
#            cnt += 1
#            
#          # on redescend d'un niveau tant que l'itérateur du niveau atteint 
#          # retourne StopIteration
#            
#            go_down = True
#            
#            if current_level > 0:
#                
#                while go_down & (current_level > 0):
#                    
#                    current_level -= 1
#                    
#                    try:
#                        current_post = list_managers[current_level].next
#                    except StopIteration:
#                        go_down = False
#                  
#            else:
#
#                cnt_found = True
#
#
#        logging.info('count_post_branchs/nombre de fils pour chaque branche : %s' % str(cnt))   
#    return cnt
    
#def count_discussion_branchs(discussion_id):
#
#    """
#        Cette fonction retourne le nombre de fils de discussion.
#        
#        L'algorithme estime ce nombre en se basant sur le nombre d'éléments de  
#        la liste des publications qui sont sans réponses
#        
#                        ATTENTION
#        
#        On pourrait penser que ce nombre est égale au nombre 
#        d'éléménts de cette liste mais ce serait oublier les fils qui naissent
#        d'une réponse à plusieurs sources
#    
#    """
#    
#    posts = get_discussion_posts_list_without_any_response(discussion_id)
#    
#    logging.info('count_discussion_branchs/Nombre de publications sans réponses : %s' % len(posts))
#  
#    cnt = 0
#    
#    for post in posts: 
#        logging.info('ENTRÉE dans la boucle FOR de count_discussion_branchs')
#        cnt += count_post_branchs(post)
#        logging.info('count_discussion_branchs/nombre de fils das la boucle for : %s' % str(cnt))   
#    return cnt


class DiscussionContext(object):
    
    def tagsdict_loading(self):
#        contents = Discussion.objects.filter(thread=self.thread)
        rcinfos, discussions = model.accessor_get_discussions_from_opened_thread(self.thread)
        if rcinfos.success:
            tags = []
            for discussion in discussions:
                
                reflexions = discussion.publication.get_reflexions()
                for reflexion in reflexions:
                    tags_content = reflexion.idea.tags.names()
                    for tag_content in tags_content:
                        tags.append(tag_content)
        
        tagsdict = {}
        for tag in tags:
            tagsdict[tag] = tagsdict.get(tag, 0) + 1
        logging.info('ThreadContext/tags_loading/contenu du dictionnaire : %s' % tagsdict)       
        self.tagsdict = tagsdict   
        return self.tagsdict
        
    def __init__(self, thread):
        
        self.thread = thread
        rcinfos, usernames, profiles = model.accessor_extract_profiles_from_opened_thread(thread.id)
        logging.info('ThreadContext/__init__/liste des users rattachés à ce fil de discussion : %s' % usernames)    
        self.usernames=usernames
        self.profiles = profiles
        self.tagsdict_loading()
        
    def get_nparticipants(self): 
        return len(self.usernames)
    def get_participants_list(self): 
        participants_list = ''
        for index in range(len(self.usernames)):
            if index < len(self.usernames)-1:
                participants_list += self.usernames[index] + ', '
            else:
                participants_list += self.usernames[index]
        return participants_list
        
    def get_first_participant(self): 
        logging.info('DiscussionContext/get_first_participant/liste des users rattachés à cette discussion : %s' % self.usernames)    
        return self.usernames[0]
        
def get_discussion(thread):

    return DiscussionContext(thread)
    

def get_discussions_list(threads):
    data = []
    for thread in threads:    
        logging.info('get_discussions/contents : %s' % get_discussion(thread))
        data.append(get_discussion(thread))
    return data

    
def extract_tags(text):
    logging.info("extract_tags/entrée dans la fonction")    
    
    # 1ère étape : remplacer tous les cacatères qui ne sont pas des mots par des blancs
    # nécessité de modifier l'encodage pour que la fonction str.maketrans fonctionne
    characters_to_eliminate= ".:'!?;/\-()0123456789,;«»"
    characters_to_replace  = "                         "

    str_elim = characters_to_eliminate.decode('utf-8','replace').encode('latin1','replace')
    str_repl = characters_to_replace.decode('utf-8','replace').encode('latin1','replace')
    table = maketrans(str_elim,str_repl)
    print 
    logging.info('extract_tags/text est-il une instance de la classe unicode ? : %s' % isinstance(text, unicode))
    
#    wtext = unicode(text, sys.stdin.encoding)
    wtext = text.encode('latin1', 'replace')
    wtext = wtext.translate(table).decode('latin1','replace')#.encode('utf-8','replace')
    
    words = wtext.split(' ')
    logging.info('extract_tags/nombre de mots : %s' % len(words))
    logging.info('extract_tags/liste des mots : %s' % words)
    
    for index in range(len(words)):
        words[index] = words[index].lower()

  # 2ème étape : supprimer les mots sans intérêts

    words_to_eliminate = [u'l', u'le', u'la',u'les', u'de', u'du', u'des',
                          u'a', u'à', u'aux', u'ce', u'ces', u'cet', u'cette',
                          u'notre', u'votre', u'nos', u'vos', u'leur', u'leurs', 
                          u'mais', u'où', u'et', u'donc', u'or', u'ni', u'car',
                          u'qu', u'que', u'qui', u'dont', u'on', u'en',
                          u'lequel', u'laquelle', u'lesquels', u'lesquelles',
                          u'quel', u'quels', u'quelle', u'quelles',
                          u'se', u'lorsqu', u'lorsque', u'jusqu', u'jusque',
                          u'certe', u'volontiers', u'dans',
                          u'je', u'tu', u'il', u'nous', u'vous', u'ils', u'elle', u'elles',
                          u'n', u'y', u'combien', u'un', u'une', u'eux', u'nous-même',
                          u'vous-même', u'eux-même', u'lui-même', u'elle-même',
                          u'quand', u'pourquoi', u'si', u'autour', u'avec',
                          u'fois', u'guise', u' ', u'  ', u'   ', u'', u'me', u'd',
                          u'ceux', u'celle', u'celui', u'celles', u'son', u'sa', u'ses',
                          u'moi', u'toi', u'soi', u'soit', u'lui', u'sûr', u'trop', u'moins',
                          u'peu', u'assez' u'beaucoup', u'sans', u's', u'pas', u'non', u'oui',
                          u'n', u'deux', u'trois', u'quatre', u'cinq', u'six', u'sept', 
                          u'huit', u'neuf', u'ne', u'suis', u'es', u'est', u'ai', u'as',
                          u'par', u'autre', u'autres', u'certain', u'certains', u'certaine',
                          u'certaines', u'même', u'entre', u'celui-ci', u'celui-là',
                          u'celles-ci', u'celles-là', u'ici', u'là', u'surtout', u'moins',
                          u're', u'mon', u'ma', u'mes', u'ton', u'ta', u'tes', u'sur', u'sous'
                          u'c',u'm',u't',u'ça'
                         ]
    logging.info('extract_tags/liste des mots à éliminer : %s' % words_to_eliminate)  
    logging.info('extract_tags/nombre de mots : %s' % len(words)) 
    
    for word in words_to_eliminate:
        while word in words:
            words.remove(word)
            
#    for word in words:
#        logging.info('extract_tags/mot courant : %s' % word)     
#        if word.lower() in words_to_eliminate:
#            words.remove(word)
#            logging.info('extract_tags/mot courant supprimé')  
#        else:
#            logging.info('extract_tags/mot courant à conserver')    
            
    logging.info('extract_tags/liste des mots : %s' % words)         
#    words = []
#    for phrase in phraseslist:
#        phrase = phrase.lower()    
#        phrase_words = phrase.split(' ')
#        logging.info('extract_tags/liste des mots de la phrase courante : %s' % phrase_words)
#        for word in phrase_words:
#            words.append(word)
        
    tags = words
    return tags
    
def tagscorrection(request, form):

    """
    Problème : Au niveau de l'application taggit, si on ne saisit qu'un seul tag et qu'il est composé de plusieurs mots, alors il sera créé autant de tags qu'il y a de mots. Pour s'affranchir de ce problème, une solution consiste à modifier, par programme, l'instance de form en lui passant une chaine unicode
    
    """

    rtags = request.POST.get('tags')
    logging.info("tagscorrection / request.POST = %s" % rtags)
    logging.info("tagscorrection / len(rtags.split(',')) = %s" % len(rtags.split(',')))
    wtags = []
    if len(rtags.split(',')) == 1:
        wtags.append(rtags.encode('utf8'))
    else:
        tags = form.cleaned_data['tags']
        for tag in tags:
            wtags.append(tag.encode('utf8'))
            
    logging.info(u"tagscorrection / tag retournés par la fonction = %s" % wtags)
        
    return wtags
    

def get_word_position_instances(word, text):
    
    """
    Ce programme retourne une liste de tuple 
    (mot, position du mot dans le texte) de chaque occurence du mot trouvée 
    dans le texte
    """
    # on s'affranchit du problème des majuscules
    wtext = text.lower()   
    wword = word.lower()   
    
    replace_by_char = " " 
    replaced_positions = []
    instances = []
    alphabetic_set = 'abcdefghijklmnopqrstuvwxyz0123456789'

    i = 0
    
    while wword in wtext:
        # recherche de la position dans le texte

        print wword
        
        pos = wtext.find(wword)
        print pos
        if pos >=0:
            
            if pos == 0:
                # instance à prendre en compte
                escape = False
            else:
                # instance à échapper, on la testera dans la boucle suivante
                # avec le blanc placé devant
                if i == 0:
                    escape = True
                else:
                    escape = False
                
            if not escape:
                
                if replaced_positions:
                # on s'assure que le blanc n'est pas un blanc généré par cet algo
                    if pos not in replaced_positions:
                        is_word_of_interest = True
                    else:
                        is_word_of_interest = False
                  
                else:
                    is_word_of_interest = True
                
    
                
                if is_word_of_interest:
            
                    # on s'assure que le caractère suivant le mot n'est pas une lettre
                    # car dans ce cas, le mot serait localisé dans un mot plus grand
            
                    if (pos + len(wword)) < len(wtext):
                        
                        if wtext[pos + len(wword)] not in alphabetic_set:
                            
                            if len(wword)>len(word):
                                instances.append((wword, pos + 1)) # - 1 à cause du blanc ajouté à wword
                            else:
                                instances.append((wword, pos))
                    
                # dans le texte, remplacement des caractères du mot par des blancs
                wwtext = list(wtext)
                for i in range(len(wword)):
                    wwtext[pos + i] = replace_by_char
                    replaced_positions.append(pos + i)
                wtext = ''.join(wwtext)
                

        wword = " " + word.lower() # to avoid word inside word
        i +=1
            
    return instances
            
            
        
        
    
    
    