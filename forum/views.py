#-*- coding: utf-8 -*-

from django.shortcuts import render
from django.views.generic import ListView, DetailView
#from forum.models import PublishedReflexion, Thread
#from forum.models import Discussion#, Reflexion, ReflexionState
import forum.models as forum_model
import quadrantides.generics as generic

import datetime
from forum.forms import PublicationForm, IdeaForm, AccountCreationForm, IdeaFilterForm
from forum.forms import UserForm, ProfileForm, ReflexionStateForm, TagForm
from django.contrib.auth.models import User
import logging
import forum.processes as process
#import forum.accessors as accessor
import forum.communications as com
import forum.tasks as task

from taggit.models import Tag
from django.views.generic.edit import FormView
from django.contrib.auth.decorators import login_required
from taggit.managers import TaggableManager

#class TagMixin(object):
#    def get_context_data(self, kwargs):
#        context = super(TagMixin, self).get_context_data(kwargs)
#        context['tags'] = Tag.objects.all()
#        return context       
##    def get_sources_count(self):
##        return self.sources_count
##    def get_post(self):
##        return self.post
#        

  
        
class PostDetail(DetailView):
    context_object_name = "post"
    model = forum_model.PublishedReflexion
    template_name = "forum/forum_publication_detail.html"
    
    def get_context_data(self, **kwargs):
        # Nous récupérons le contexte depuis la super-classe
        context = super(PostDetail, self).get_context_data(**kwargs)

        post = context['post']  

        context['post_context'] = process.get_post_context(post)
#        logging.info('PostDetail/get_context_data/liste des tags : %s' % type(post.reflexion.idea.tags.get_queryset()))
#        context['tags'] = post.get_tag_names()
        rcinfos = generic.get_returned_code_ok_object()
        context['error_context'] = rcinfos
        context.pop('post')
        logging.debug(context)
        return context
        
############################################################################### 
        
        
class CurrentParticipantsList(ListView):
	model = User
	context_object_name = "current_participants"
	template_name = "forum/forum_last_news.html"

###############################################################################
class ThreadsList(ListView):
     
     model = forum_model.Thread
     context_object_name = "all_opened_threads"
     template_name = "forum/forum_all_threads.html"
     paginate_by = 3
     queryset = forum_model.Thread.objects.filter(is_opened = True).order_by('-date')
 
     def get_context_data(self, **kwargs):
         
         context = super(ThreadsList, self).get_context_data(**kwargs)
         rcinfos, all_profiles_in_opened_threads = forum_model.accessor_extract_profiles_from_opened_threads()
         context['all_profiles_in_opened_threads'] = all_profiles_in_opened_threads
         threads = context['all_opened_threads']
#         contents = process.get_discussions_list(threads)
#         logging.info('ThreadsList/get_context_data/contents : %s' % contents)
         context['discussions_context'] = process.get_discussions_list(threads)
         context.pop('all_opened_threads')
         context['error_context'] = rcinfos
         
         return context
         
###############################################################################     
#class ThreadsListInPersonalSpaceContext(ListView):
#     
#     model = Thread
#     context_object_name = "all_opened_threads"
#     template_name = "forum/forum_personal_space_threads.html"
#     paginate_by = 3
#     queryset = Discussion.objects.filter(is_opened = True).order_by('-date')
# 
#     def get_context_data(self, **kwargs):
#         
#         context = super(DiscussionsListInPersonalSpaceContext, self).get_context_data(**kwargs)
#         all_profiles_in_opened_discussions = process.get_all_profiles_in_opened_discussions()
#         context['all_profiles_in_opened_discussions'] = all_profiles_in_opened_discussions
#         discussions = context['all_opened_discussions']
#         contents = process.get_discussions_content_list(discussions)
#         logging.info('DiscussionsList/get_context_data/contents : %s' % contents)
#         context['discussions_content'] = process.get_discussions_content_list(discussions)
#         context.pop('all_opened_discussions')
#
#         return context
        
###############################################################################      
class NewlyPublishedReflexions(ListView):

    logname = "C:\Users\Patrice\Documents\Patrice\Travail\missions\python2.7\django\site_internet\quadrantides\log.txt"
    logging.basicConfig(filename=logname,
                                filemode='a',
                                format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
#                                datefmt='%H:%M:%S',
                                level=logging.DEBUG)

    logging.info('PostsList/La date courante est : %s' % datetime.datetime.today())
    d = datetime.datetime.today() - datetime.timedelta(days=7)
    logging.info('PostsList/La date de la semaine dernière est : %s' % d)
    
#    q = PublishedReflexion.objects.get(id=1)
#    logging.info('PostsList/La date de publication dans la DB pour id=1 est : %s' % q.date_publication)    
    model = forum_model.PublishedReflexion
    context_object_name = "recent_posts"
    template_name = "forum/forum_last_news.html"
    paginate_by = 3
    queryset = forum_model.PublishedReflexion.objects.filter(date__gt = datetime.datetime.today() -datetime.timedelta(days=7)).order_by('-date')


    def get_context_data(self, **kwargs):
        # Nous récupérons le contexte depuis la super-classe
        context = super(NewlyPublishedReflexions, self).get_context_data(**kwargs)
        rcinfos, all_profiles_in_opened_threads = forum_model.accessor_extract_profiles_from_opened_threads()
        context['all_profiles_in_opened_threads'] = all_profiles_in_opened_threads

        posts = context['recent_posts']  

        context['recent_posts'] = process.get_post_context_list(posts)
        context['error_context'] = rcinfos
        logging.debug(context)
        return context

###############################################################################
#def display_thread(request, id):
#	posts = PublishedReflexion.objects.filter(thread__id=id).order_by('-date_publication')
#
#	return render(request, 'forum/forum_discussion_detail.html', {'posts': posts})
#
################################################################################
def create_publication_form(request, id, afficher_source):
    
    rinfos, profile = task.get_profile(request)
    
    success = rinfos.success

    if success:
        
        
        post = forum_model.PublishedReflexion.objects.get(id=id)
        post_context = process.get_post_context(post)
        logging.info("create_publication_form/request.method = %s", request.method)
        logging.info("create_publication_form/afficher_source = %s", afficher_source)
        
        if request.method == 'POST':
            form = PublicationForm(request.POST)
            logging.info("create_publication_form/valide ? %s", form.is_valid())
            
            if form.is_valid():
                
                is_valid_form = True
                
                tags = process.tagscorrection(request, form)    
                
                # gestion de l'instance Idea : création si nouvelle
                
                text = form.cleaned_data['text']
                in_new_discussion = form.cleaned_data['in_new_discussion']
                logging.info("create_publication_form/in_new_discussion = %s", in_new_discussion)
                
                if in_new_discussion:
                    
                    rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                                           profile, 
                                                                           tags = tags) 
                                                                             
                                                                             
                else:
                    
                    rcinfos, discussion = task.publish_idea_in_existing_thread(text, 
                                                                                profile, 
                                                                                source = post,
                                                                                tags = tags)
                    
                if rcinfos.success:
                    
                    technical_problem = False

                else:
                    technical_problem = True
                    

            else:
                display=True
                technical_problem = False
                is_valid_form = False                
    
        else:
            is_valid_form = False
            form = PublicationForm()
            technical_problem = False
  
                      
    else:
        technical_problem = True

          
    if technical_problem:
        
        logging.info(rinfos.message)
        tplcontext = {'error_context': rinfos}
        
    else:

        if afficher_source=='1':
            display = True
        else:
            display=False
                
        tplcontext = {'profile': profile, 
                      'is_valid_form': is_valid_form, 
                      'display' : display,
                      'form':form,
                      'error_context': rinfos,
                      'post_context':post_context} 
                      
                      
    return render(request, 'forum/forum_formulaire_publication.html', tplcontext)
###############################################################################   
def create_thread_form(request):
    """
    Création de la page "Ouverture d'un nouveau fil de discussion"
    """    
    
    rinfos, profile = task.get_profile(request)
    
    success = rinfos.success

    if success:
        
        if request.method == 'POST':
            
            form = IdeaForm(request.POST)
            logging.info("create_thread_form/Réponse du formulaire de publication/valide ? %s", form.is_valid())
            
            if form.is_valid():
                
                is_valid_form = True
                tags = process.tagscorrection(request, form)
                logging.info("create_thread_form / tags %s", type(tags[0]))  
                text = form.cleaned_data['text']
                
                rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                                       profile, 
                                                                       tags = tags)
                                                                         
                if rcinfos.success:
                    
                    technical_problem = False

                else:
                    technical_problem = True

            else:
                technical_problem = False
                is_valid_form = False
                
        else:
            form = IdeaForm()
            technical_problem = False    
            is_valid_form = False
                  
    else:
        technical_problem = True

        
    if technical_problem:
        
        logging.info(rinfos.message)
        tplcontext = {'error_context': rinfos}
        
    else:
        
        tplcontext = {'profile': profile, 
                      'form': form,
                      'is_valid_form': is_valid_form,
                      'error_context': rinfos}
    
    return render(request, 'forum/forum_form_new_discussion.html', tplcontext)

###############################################################################   
def create_simple_reflexion_form(request):
    """
    Création de la page "création d'une réflexion simple dont l'idée 
    n'existe pas encore
    """    
    
    rinfos, profile = task.get_profile(request)
    
    success = rinfos.success

    if success:
        
        if request.method == 'POST':
            
            form = IdeaForm(request.POST)
            
            if form.is_valid():
                
                is_valid_form = True
                tags = process.tagscorrection(request, form)
                logging.info("create_thread_form / tags %s", type(tags[0]))  
                text = form.cleaned_data['text']
                
                rcinfos, discussion = task.create_reflexion(text, 
                                                            profile, 
                                                            tags = tags)
                                                                         
                if rcinfos.success:
                    
                    technical_problem = False

                else:
                    technical_problem = True

            else:
                technical_problem = False
                is_valid_form = False
                
        else:
            form = IdeaForm()
            technical_problem = False    
            is_valid_form = False
                  
    else:
        technical_problem = True

        
    if technical_problem:
        
        logging.info(rinfos.message)
        tplcontext = {'error_context': rinfos}
        
    else:
        
        tplcontext = {'profile': profile, 
                      'form': form,
                      'is_valid_form': is_valid_form,
                      'error_context': rinfos}
    
    return render(request, 'forum/forum_form_new_simple_reflexion.html', tplcontext)
    
###############################################################################   
def create_compounded_reflexion_form(request):
    """
    Création de la page "création d'une réflexion composée d'idées existantes"
    """    

    rinfos, profile = task.get_profile(request)
    
    success = rinfos.success

    if success:
                
        if request.method == 'POST':
            
            form = IdeaFilterForm(request.POST)
            
            if form.is_valid():
                
                is_valid_form = True
                
#                tags = process.tagscorrection(request, form)
#                logging.info("create_thread_form / tags %s", type(tags[0]))  
                text = form.cleaned_data['text']                
                requested_words = text.split(" ")

                wrequested_users = form.cleaned_data['requested_users']      
                logging.info("create_compounded_reflexion_form / wrequested_users : <%s>", wrequested_users) 
                requested_users = generic.returned_var_as_list(wrequested_users.split(" ")) 

                wexcluded_users = form.cleaned_data['excluded_users']                
                excluded_users = generic.returned_var_as_list(wexcluded_users.split(" "))
                ideas_filter = com.IdeasContainer(words = requested_words, 
                                                  requested_users = requested_users,
                                                  excluded_users = excluded_users)
                                                  
                rcinfos, context = ideas_filter.get_context_data() 
                                                                
                if rcinfos.success:

                    technical_problem = False       
                    tplcontext = {'profile': profile, 
                                  'form': form,
                                  'context':context,
                                  'nresults': len(context),
                                  'is_valid_form': False,
                                  'error_context': rinfos}
                              
                else:
                    technical_problem = True

            else:
                technical_problem = False
                is_valid_form = False 
                tplcontext = {'profile': profile, 
                              'form': form,
                              'is_valid_form': False,
                              'error_context': rinfos}
                                  
        else:
            form = IdeaFilterForm()
#            form.fields['username'].choices = ideas_filter.get_username_choices()
            ideas_filter = com.IdeasContainer()
            rcinfos, context = ideas_filter.get_context_data() 
                
            technical_problem = False    
            is_valid_form = False
            
            tplcontext = {'profile': profile, 
                          'form': form,
                          'is_valid_form': is_valid_form,
                          'error_context': rinfos}
                      
                  
    else:
        technical_problem = True

        
    if technical_problem:
        
        logging.info(rinfos.message)
        tplcontext = {'error_context': rinfos}
        

    return render(request, 'forum/forum_form_new_compounded_reflexion.html', tplcontext)

###############################################################################
class AccountCreationView(FormView):
    """
    Création de la page "Saisie des donnée pour la création d'un nouveau
                         compte utilisateur"
    """    
    
    template_name = "forum/forum_formulaire_creation_compte.html"
    form_class = AccountCreationForm
    success_url = 'succes'

    def get_context_data(self, **kwargs):
        # Nous récupérons le contexte depuis la super-classe
        context = super(AccountCreationView, self).get_context_data(**kwargs)
        context['error_context'] = {'success':True}

        return context
        
    def form_valid(self, form):
        logging.info("AccountCreationView/la saisie est-elle valide ? %s", form.is_valid())
        if form.is_valid():
     
            for f in form.form_classes:
                name = f.__name__.lower()
                fform = getattr(form, name)
                if type(fform) is UserForm:              
                    uform = fform
#                    cleaned_data = form.clean()
#                    site_web = form.cleaned_data['site_web']
                    new_user = uform.save()
                    new_user.is_active=True
                    new_user.set_password(uform.cleaned_data['password'])
                    new_user.save()
                    
                    logging.info("AccountCreationView/classe de la form ? %s", type(new_user))
                    logging.info("AccountCreationView/user id = %s", new_user.id)
                    
                else:
                    if type(fform) is ProfileForm:

                        pform = fform
                        profile= new_user.profile
                        logging.info("AccountCreationView/pform = %s", pform)
                        avatar = form.cleaned_data['avatar']
                        site_web = form.cleaned_data['site_web']
                        profile.avatar = avatar
                        profile.site_web = site_web
                        profile.save()
                          
        return super(AccountCreationView, self).form_valid(form)   

###############################################################################    
def create_account_success(request):  
    """
    Création de la page "La création du compte demandé est un succès"
    """                               
    return render(request, 'forum/forum_creation_compte_succes.html',
                           {'error_context':{'success':True}})
         
###############################################################################       
@login_required(login_url="/connexion/espace/personnel/")  
def personal_space_display(request):
    
    """
    Création de la page "espace personnel d'un Utilisateur"
            Page par défaut
    
    """
    # récupération du profil de l'utilisateur
    # anonymous si pas aunthentifié
    
#    if request.user.is_anonymous():
#        user = User.objects.get(username='anonymous')
#        profile = user.profile
#    else:
#        profile = request.user.profile
    if request.user.is_anonymous():
        username='anonymous'
        rcinfos, profile = forum_model.accessor_get_profile(username)
    else:
        profile = request.user.profile
        rcinfos = generic.get_returned_code_ok_object()
        
    ref_com = com.ReflexionsPerProfileCom(request.user.profile)
    context = ref_com.get_context_data()
    
    rcinfos, categories = forum_model.accessor_get_profile_reflexion_categories(request.user.profile)

    if rcinfos.success:
        if request.method == 'POST':
            
            form = TagForm(request.POST)
            
            if form.is_valid():
                
                is_valid_form = True
                tagname = form.cleaned_data['name']
                form.save()
            
                form = TagForm()
                tplcontext = {'categories': categories, 
                              'context' : context,
                              'error_context': rcinfos,
                              'profile':profile,
                              'category_form': form}
                          
        else:

            form = TagForm()
            tplcontext = {'categories': categories, 
                          'context' : context,
                          'error_context': rcinfos,
                          'profile':profile,
                          'category_form': form}
    else:
        tplcontext = {'error_context': rcinfos}

    return render(request, 
                 'forum/forum_personal_space.html', tplcontext)    
###############################################################################       
@login_required(login_url="/connexion/espace/personnel/")  
def personal_space_overview(request):
    
    """
    Création de la page "espace personnel d'un Utilisateur"
            Page par défaut
    
    """
    # récupération du profil de l'utilisateur
    # anonymous si pas authentifié
    
    if request.user.is_anonymous():
        username='anonymous'
        rcinfos, profile = forum_model.accessor_get_profile(username)
    else:
        profile = request.user.profile
        rcinfos = generic.get_returned_code_ok_object()
        
    ref_com = com.ReflexionsPerProfileCom(request.user.profile)
    context = ref_com.get_without_category_context_data()
    
    rcinfos, categories = forum_model.accessor_get_profile_reflexion_categories(request.user.profile)

    if rcinfos.success:
        if request.method == 'POST':
            
            form = TagForm(request.POST)
            
            if form.is_valid():
                
                is_valid_form = True
                tagname = form.cleaned_data['name']
                form.save()
            
                form = TagForm()
                tplcontext = {'categories': categories, 
                              'context' : context,
                              'error_context': rcinfos,
                              'profile':profile,
                              'category_form': form}
                          
        else:

            form = TagForm()
            tplcontext = {'categories': categories, 
                          'context' : context,
                          'error_context': rcinfos,
                          'profile':profile,
                          'category_form': form}
    else:
        tplcontext = {'error_context': rcinfos}

    return render(request, 
                 'forum/forum_personal_space_overview.html', tplcontext)  
###############################################################################       
@login_required(login_url="/connexion/espace/personnel/")  
def personal_space_new_theme(request):
    
    """
    Création de la page "espace personnel d'un Utilisateur"
            Page par défaut
    
    """
    # récupération du profil de l'utilisateur
    # anonymous si pas authentifié
    
    if request.user.is_anonymous():
        username='anonymous'
        rcinfos, profile = forum_model.accessor_get_profile(username)
    else:
        profile = request.user.profile
        rcinfos = generic.get_returned_code_ok_object()
        
    ref_com = com.ReflexionsPerProfileCom(request.user.profile)
    context = ref_com.get_context_data()
    
    rcinfos, categories = forum_model.accessor_get_profile_reflexion_categories(request.user.profile)

    if rcinfos.success:
        if request.method == 'POST':
            
            form = TagForm(request.POST)
            
            if form.is_valid():
                
                is_valid_form = True
                tagname = form.cleaned_data['name']
                form.save()
            
                form = TagForm()
                tplcontext = {'categories': categories, 
                              'context' : context,
                              'error_context': rcinfos,
                              'profile':profile,
                              'category_form': form}
                          
        else:

            form = TagForm()
            tplcontext = {'categories': categories, 
                          'context' : context,
                          'error_context': rcinfos,
                          'profile':profile,
                          'category_form': form}
    else:
        tplcontext = {'error_context': rcinfos}

    return render(request, 
                 'forum/forum_personal_space_new_theme.html', tplcontext)    

###############################################################################

@login_required(login_url="/connexion/espace/personnel/")  
def personal_space_display_with_reflexion_state_form(request, index, action):
    
    """
    Création de la page "espace personnel d'un Utilisateur"
        Page incluant le formulaire "Faire évoluer"
    
    
    """
#    if request.user.is_anonymous():
#        user = User.objects.get(username='anonymous')
#        profile = user.profile
#    else:
#        profile = request.user.profile
        
    if request.user.is_anonymous():
        username='anonymous'
        rcinfos, profile = forum_model.accessor_get_profile(username)

    else:
        profile = request.user.profile
        rcinfos = generic.get_returned_code_ok_object() 
        
        
        
    ref_com = com.ReflexionsPerProfileCom(request.user.profile)
    if action == u"évolution":
        requested_action_index=1
    else:
        if action == u"modify_category":
            requested_action_index=2
        else:        
            requested_action_index=0
            
    ref_com.set_requested_action_index(int(index), requested_action_index) 
    context = ref_com.get_context_data() 
    rcinfos, categories = forum_model.accessor_get_profile_reflexion_categories(request.user.profile)
    if rcinfos.success:
        if request.method == 'POST':
            logging.info("personal_space_display_with_reflexion_state_form/Réponse du formulaire de publication/request.method == 'POST' ")
            unicodestate = request.POST.get('state', '')
            state = unicodestate.encode('utf8')
            logging.info(u"personal_space_display_with_reflexion_state_form/Réponse du formulaire de publication/selection = %s", state)
            reflexion_state = context[int(index)].state
            reflexion_state.state = state
            reflexion_state.save()
            # suppression de l'action puisqu'elle vient d'être traitée
            ref_com.set_requested_action_index(int(index), 0) 
            context = ref_com.get_context_data()
            tplcontext = {'categories': categories,
                          'error_context': rcinfos,
                          'context' : context,
                          'profile':profile}
    
        else:
            state_label = context[int(index)].get_label_prefix()
            state_help_text = context[int(index)].get_label_suffix()
            logging.info("personal_space_display_with_reflexion_state_form/context = %s", context)  
            logging.info(u"personal_space_display_with_reflexion_state_form/state_label = %s", state_label)
            logging.info(u"personal_space_display_with_reflexion_state_form/state_help_text = %s", state_help_text)
            form = ReflexionStateForm(instance = context[int(index)].state, state_label=state_label, state_help_text=state_help_text)
            form_modify_category = ReflexionStateForm(instance = context[int(index)].state, state_label=state_label, state_help_text=state_help_text)
            tplcontext = {'categories': categories, 
                          'context' : context, 
                          'form': form,
                          'profile':profile,
                          'form_modify_category': form_modify_category,
                          'error_context': rcinfos}
    else:
        tplcontext = {'error_context': rcinfos}
        
        

    
    return render(request, 'forum/forum_personal_space.html', tplcontext)  
    
###############################################################################
    
@login_required(login_url="/connexion/espace/personnel/")  
def personal_space_display_by_category(request, slug):

    tag = Tag.objects.get(slug = slug)
    rcinfos, categories = forum_model.accessor_get_profile_reflexion_categories(request.user.profile)
    if rcinfos.success:
        ref_com = com.ReflexionsPerProfilePerCategoryCom(request.user.profile, slug)
        context = ref_com.get_context_data()
        tpl_context = {'tag': tag, 'categories': categories, 'context' : context, 'error_context': rcinfos}
    else:
        tpl_context = {'error_context': rcinfos}    
    
    return render(request, 'forum/forum_personal_space.html', tpl_context) 

###############################################################################
def forum_threads_overview(request):
    if request.user.is_anonymous():
        username='anonymous'
        rcinfos, profile = forum_model.accessor_get_profile(username)
#        user = User.objects.get(username='anonymous')
#        profile = user.profile
    else:
        profile = request.user.profile
        rcinfos = generic.get_returned_code_ok_object()
        
    return render(request, 
                  'forum/forum_threads_overview.html', 
                  {'profile':profile,
                   'error_context': rcinfos})   
                   
###############################################################################
def home(request):
    if request.user.is_anonymous():
        username='anonymous'
        rcinfos, profile = forum_model.accessor_get_profile(username)
#        user = User.objects.get(username='anonymous')
#        profile = user.profile
    else:
        profile = request.user.profile
        rcinfos = generic.get_returned_code_ok_object()
           
    return render(request, 
                  'forum/forum_home.html', 
                  {'profile':profile,
                   'error_context': rcinfos})   
 
###############################################################################   
def personal_space_home(request):
                                 
    return render(request, 'forum/forum_personal_space_home.html', locals()) 
    
###############################################################################   
def subjectivity_overview_display(request):
    """
    Il s'agit de la vue qui permet d'accéder à la liste des subjectivités publiques
    ATTENTION : il se peut qu'un utilisateur n'est pas de réflexions matures
    et aucune publication, il faut le tester ici avant de retourner son username
    au template
    """
    rcinfos, users = forum_model.accessor_get_users()
            
    return render(request, 
                  'forum/forum_subjectivity_overview.html', 
                  {'users':users,
                   'error_context': rcinfos}) 
                   
###############################################################################   
def mature_reflexions_display(request, index):
    """
    Il s'agit de la vue qui permet d'accéder à la liste des réflexions matures
    des utilisateurs enregistrés, autrement dit à leur subjectivité publique
    """
    user = User.objects.get(id=index)
    
    ref_com = com.ReflexionsPerProfileCom(user.profile)
    context = ref_com.get_mature_state_context_data()
    rcinfos = generic.get_returned_code_ok_object()
    logging.info(u"mature_reflexions_display/rcinfos.success = %s", rcinfos.success)
    logging.info(u"mature_reflexions_display/context = %s", context)
    return render(request, 
                  'forum/forum_user_subjectivity.html', 
                  {'user':user,
                  'context':context,
                   'error_context': rcinfos}) 
    