# -*- coding: utf-8 -*-
"""
Created on Sun Nov 30 06:59:38 2014

@author: Patrice
"""
###############################################################################
from django.db import transaction
import django.db as db

import logging

import quadrantides.generics as generic

import forum.models as model

###############################################################################

def get_profile(request):
    
    name = u"task.get_profile"
    
    # récupération du profil de l'utilisateur
    # anonymous si pas aunthentifié
    
    if request.user.is_anonymous():
        rcinfos, profile = model.accessor_get_profile('anonymous')
        if rcinfos.success:
            pass
        else:
            profile = []
            
    else:
        profile = request.user.profile
        success, code, message = generic.returned_code_ok()
        rcinfos = generic.ReturnedCode(name, success, code, message)
    
    return rcinfos, profile

############################################################################### 
def create_reflexion(text, profile, **kwargs):

#    name = u"task.publish_idea"
    
    # gestion de l'instance Idea : création si nouvelle

    rcinfos, idea = model.accessor_create_idea(text, **kwargs)
        
    if rcinfos.success:
        
        # créer la réflexion
        # création automatique (via un signal) d'un enregistrement dans 
        # la table ReflexionState (historique des états des réflexions)
    
        rcinfos, reflexion = model.accessor_create_reflexion(idea, 
                                                             profile,
                                                             **kwargs)
        
            
    if rcinfos.success:
        pass
    else:
        reflexion = []
            
    return rcinfos, reflexion
    
    
############################################################################### 
def publish_idea(text, profile, **kwargs):

#    name = u"task.publish_idea"
    
    rcinfos, reflexion = create_reflexion(text, profile, **kwargs)
     
    if rcinfos.success:
        
        logging.info('publish_idea/type(reflexion) : %s' % reflexion)
        
        # créer la publication
            
        rcinfos, publication = model.accessor_create_publication(reflexion, **kwargs)

        logging.info('publish_idea/type(publication) : %s' % publication)
    

            
    if rcinfos.success:
        pass
    else:
        publication = []
            
    return rcinfos, publication
            
###############################################################################    
@transaction.atomic
def publish_idea_in_new_thread(text, profile, **kwargs):

    name = u"task.publish_idea_in_new_thread"

    try:
        
        with transaction.atomic():
            
            rcinfos, publication = publish_idea(text, profile, **kwargs)
            
            if rcinfos.success: 
        
                # créer le nouveau fil de discussion 
            
                rcinfos, thread = model.accessor_create_thread()
         
                if rcinfos.success:
                    
                    logging.info('publish_idea_in_new_thread/type(publication) : %s' % publication)
                            
                    # associer la publication au fil de discussion
                    # <=> créer une discussion
                                                             
                    publication_order = 1
                    rcinfos, discussion  = model.accessor_create_discussion(
                                                publication, 
                                                thread, 
                                                publication_order,
                                                **kwargs) 
                                                        
                                                        
            if rcinfos.success: 
                success = rcinfos.success
                code = rcinfos.code
                message = rcinfos.message 
            else:
                success, code, message = generic.returned_code_ko_task_aborted()
                
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message    
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message    
                
    if success == False:
        rcinfos = generic.ReturnedCode(name, success, code, message)
        discussion = []
        
    return rcinfos, discussion
    
###############################################################################    
@transaction.atomic
def publish_idea_in_existing_thread(text, profile, **kwargs):

    name = u"task.publish_idea_in_discussion"

    try:
        
        with transaction.atomic():
    
            rcinfos, publication = publish_idea(text, profile, **kwargs)
            
            if rcinfos.success: 
                
                # si la publication a une source, il faut la stocker   
                if kwargs:
                    
                    if 'source' in kwargs:
                        
                        publication_source_not_found = False
                        
                        publication_source = kwargs['source']
                
                        # associer la publication au(x) fil(s) de discussion de la 
                        # publication source
                        rcinfos, publication_source_discussions = model.accessor_get_discussions_for_requested_publication(publication_source)    
                                    
                        if rcinfos.success:
                                      
                            for publication_source_discussion in publication_source_discussions:          
                                publication_order = publication_source_discussion.publication_order + 1
                                rcinfos, discussion = model.accessor_create_discussion(publication, 
                                                                           publication_source_discussion.thread, 
                                                                           publication_order,
                                                                           **kwargs)
                                                                   
                    else:
                        publication_source_not_found = True
                                                           
                else:
                    publication_source_not_found = True
                    
        
                if publication_source_not_found:
                    success, code, message = generic.returned_code_ko_task_aborted()
                else:
                    success = rcinfos.success
                    code = rcinfos.code
                    message = rcinfos.message                
                                               
            else:
                success = rcinfos.success
                code = rcinfos.code
                message = rcinfos.message
                                                        

    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message    
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message    
                
    if success == False:
        discussion = []
    
    rcinfos = generic.ReturnedCode(name, success, code, message)
                                                                    
    return rcinfos, discussion