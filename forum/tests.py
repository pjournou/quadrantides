# -*- coding: utf-8 -*-
"""
Created on Fri Oct 31 19:55:45 2014

@author: Patrice
"""

# -*- coding: utf-8 -*-

###############################################################################
import forum.communications as com
from django.test import TestCase, RequestFactory
from django.test import Client
from django.contrib.auth.models import User, AnonymousUser
from django.core.urlresolvers import reverse

import django.db as db

import forum.models as model
import forum.tasks as task
import forum.processes as process
import forum.views as view

import quadrantides.generics as generic


################################################################################
#class UserTests(TestCase):
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#       
#        user, iscreation = User.objects.get_or_create(username=u"anonymous")
#        
#    def test_creation(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification que l'enregistrement demandé a bien été créé
#        """
#        username = u"orion"
#        rcinfos, user = model.accessor_create_user(username=username)
#        if rcinfos.success:
#            user2 = User.objects.get(username=username)
#            self.assertEqual(username, str(user2.username))
#        
#    def test_creation_profile_automatically(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification qu'un profil est automatiquement enregistré dans la
#        table Profil à la création d'un nouvel enregistrement dans la 
#        table User
#        """
#        username = u"anonymous"
#        
#        user = User.objects.get(username=username)
#        
#        # On s'assure qu'un profile a bien été créé
#        
#        profile = user.profile
#        
#        self.assertEqual(profile, model.Profile.objects.get(user=user))    
#        
#    def test_get_profile(self):
#        """
#        exemple de test qui remonte l'exception : DoesNotExist
#        (Demande d'un enregistrement qui n'existe pas dans la table User)
#        """
#        self.assertRaises(User.DoesNotExist, 
#                          User.objects.get, username=u"toto")
#                          
################################################################################
#class IdeaTests(TestCase):
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#        text = u"idee #1"
#        idea = model.Idea(text=text)
#        idea.save()
#
#    def test_creation(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification que l'enregistrement demandé a bien été créé
#        """
#        text = u"idee #2"
#        idea = model.Idea()
#        idea.text = u"idee #2"
#        idea.save()
#        
#        idea2 = model.Idea.objects.get(text=text)
#        self.assertEqual(text, str(idea2.text))
#        
#        
#    def test_creation_same_one(self):
#        """
#        exemple de test qui remonte l'exception : IntegrityError
#        (Demande de création d'un enregistrement qui existe déjà dans la table 
#        Idea)
#        
#        """
#        text = u"idee #1"
##        idea = model.Idea(text=text)
##        idea.save()
#
#        idea2 = model.Idea(text=text)
#        self.assertRaises(db.IntegrityError, idea2.save)
#
#    def test_add_same_tag_twice(self):
#        """
#        on s'assure que la méthode add ne renvoie aucune exception
#        lorsqu'on essaie d'ajouter un tag qui existe déjà dans la table
#        la version actuelle de TaggableManager gère cela
#        
#        """
#        idea = model.Idea.objects.get(text = u"idee #1")
#        tag = u"tag #1"
#        ltag = []
#        ltag.append(tag)
#        idea.tags.add(tag)
#        idea.tags.add(tag)
#        self.assertListEqual(list(idea.tags.names()),ltag)
#                          
################################################################################
#class ReflexionTests(TestCase):
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#        # Création d'une réflexion
#        #   association Profil «Anonymous» - Idea «u"idée #1"»
#        user = User(username=u"anonymous")
#        user.save()
#        profile = model.Profile.objects.get(user=user)
#
#        rcinfos, idea = model.accessor_create_idea(u"idee #1", tags = u"tag #1")
#        if rcinfos.success:
#            rcinfos, reflexion = model.accessor_create_reflexion(idea, profile)
#
#            
#    def test_creation(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification que l'enregistrement demandé a bien été créé
#        """    
#        user = User.objects.get(username=u"anonymous")
#        rcinfos, idea = model.accessor_create_idea(u"idee #2", tags = u"tag #1")
#        if rcinfos.success:
#            rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile)
#            if rcinfos.success:
#                categories = 'categorie #1,'                                    
#                reflexion.categories.add(categories)
#
#                self.assertRaises(db.IntegrityError, reflexion.save())
#        
#                reflexion2 = model.Reflexion.objects.get(idea=idea, profile=user.profile)
#              
#                self.assertEqual(reflexion2.idea, reflexion.idea)
#                self.assertEqual(reflexion2.profile, reflexion.profile)
#        
#        
#        
##    def test_creation_same_reflexion(self):
##        """
##        exemple de test qui remonte l'exception : IntegrityError
##        (Demande de création d'un enregistrement qui existe déjà dans la table 
##        Idea)
##        
##        """      
##        user = User.objects.get(username=u"anonymous")
##        text=u"idée #1"
##        idea = model.Idea.objects.get(text=text)
##        reflexion = model.Reflexion()
##        reflexion.profile = user.profile
##        reflexion.idea = idea
##        self.assertRaises(db.IntegrityError, reflexion.save)
#
#        
#    def test_reflexion_state_default_immature(self):
#        user = User.objects.get(username=u"anonymous")
#        text=u"idee #1"
#        idea = model.Idea.objects.get(text=text)
#        reflexion = model.Reflexion.objects.get(idea=idea, profile=user.profile)
#        reflexion_state = model.ReflexionState.objects.get(reflexion=reflexion)
#        
#        self.assertEqual(reflexion_state.state, "immature")
#            
#
################################################################################
#class AccessorUserTests(TestCase):
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#       
#        username = u"anonymous"
#        rcinfos, user = model.accessor_create_user(username=username)
#        
#    def test_accessor_get_users(self):
#        
#        rcinfos, users = model.accessor_get_users()
#        if rcinfos.success:
#            self.assertEqual(len(users), 1)
#            self.assertEqual(users[0].username, u"anonymous")            
#            
#            rcinfos, newuser = model.accessor_create_user(username="orion")
#            self.assertTrue(rcinfos.success)            
#            if rcinfos.success:
#                rcinfos, users = model.accessor_get_users()
#                self.assertTrue(rcinfos.success) 
#                if rcinfos.success:
#                    self.assertEqual(len(users), 2)     
#                    self.assertEqual(users[0].username, u"anonymous")    # orderby username                  
#                    self.assertEqual(users[1].username, u"orion")             
#            
#    def test_accessor_create_user(self):
#        """
#        Vérification que l'enregistrement demandé a bien été créé
#        """
#        username = u"orion"
#        rcinfos, user = model.accessor_create_user(username=username)
#        if rcinfos.success:
#            user2 = User.objects.get(username=username)
#            self.assertEqual(username, user2.username)
#            
#            
#            
#    def test_profile_created_automatically(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification qu'un profil est automatiquement enregistré dans la
#        table Profil à la création d'un nouvel enregistrement dans la 
#        table User
#        """
#        username = u"anonymous"
#        
#        user = User.objects.get(username=username)
#        
#        # On s'assure qu'un profile a bien été créé
#        
#        profile = user.profile
#        
#        self.assertEqual(profile, model.Profile.objects.get(user=user))    
#        
#    def test_accessor_get_profile_not_found(self):
#        """
#        exemple de test qui remonte l'exception : DoesNotExist
#        (Demande d'un enregistrement qui n'existe pas dans la table User)
#        """
#        username=u"toto"
#        rcinfos, profile = model.accessor_get_profile(username)
#        self.assertEqual(rcinfos.code, 'DoesNotExist')  
#        
#        
################################################################################
#class AccessorIdeaTests(TestCase):     
#        
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#        text = u"idee #1"
#        tags = u"tag #1"
#        rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#
#    def test_accessor_create_idea_ok(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification que l'enregistrement demandé a bien été créé
#        """
#        text = u"idee #2"
#        tags = u"tag #2"
#        rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#        if rcinfos.success:   
#            idea2 = model.Idea.objects.get(text=text)
#            self.assertEqual(text, idea2.text)
#        
#    def test_accessor_create_idea_already_in_db(self):
#        """
#        exemple de test qui remonte l'exception : IntegrityError
#        (Demande de création d'un enregistrement qui existe déjà dans la table 
#        Idea)
#        
#        """
#        text = u"idee #1"
#        tags = u"tag #1"
#        rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#        if rcinfos.success:   
#            success2, code2, message2 = generic.returned_code_ok_already_in_db()
#            self.assertEqual(str(rcinfos.message), str(message2))
# 
#
#    def test_add_same_tag_twice(self):
#        """
#        On s'assure que la méthode add ne renvoie aucune exception
#        lorsqu'on essaie d'ajouter un tag qui existe déjà dans la table.
#        La version actuelle de TaggableManager gère cela
#        
#        """
#        idea = model.Idea.objects.get(text = u"idee #1")
#        tags = idea.tags.names()
#        for tag in tags:
#            idea.tags.add(tag)
#        tags2 = idea.tags.names() 
#
#        self.assertEqual(list(tags),list(tags2))
#        
#    def test_accessor_get_ideas(self):
#        
#        text = u"idee #2"
#        tags = u"tag #2"
#        rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#        
#        if rcinfos.success:
#                
#            text = u"idee #3"
#            tags = u"tag #3"
#            rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#
#            if rcinfos.success:
#                
#                rcinfos, ideas = model.accessor_get_ideas()  
#                self.assertTrue(rcinfos.success)
#                if rcinfos.success:
#                    self.assertEqual(len(ideas), 3) # one created in setup
#                
################################################################################
#class AccessorReflexionTests(TestCase):        
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#        # Création d'une réflexion
#        #   association Profil «Anonymous» - Idea «u"idée #1"»
#
#        username = u"anonymous"
#        rcinfos, user = model.accessor_create_user(username=username)
#        if rcinfos.success:
#            text = u"idee #1"
#            tags = u"tag #1"
#            rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#            if rcinfos.success:
#                rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile)  
#        
#    def test_accessor_create_reflexion_ok(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification que l'enregistrement demandé a bien été créé
#        """    
#
#        username = u"orion"
#        rcinfos, user = model.accessor_create_user(username=username)
#        if rcinfos.success:
#            text = u"idee #2"
#            tags = u"tag #2"
#            rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#            if rcinfos.success:
#                categories = 'categorie #2'   
#                rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile, categories = categories)
#                if rcinfos.success:            
#                    reflexion2 = model.Reflexion.objects.get(idea=idea, profile=user.profile)
#                  
#                    self.assertEqual(reflexion2.idea, reflexion.idea)
#                    self.assertEqual(reflexion2.profile, reflexion.profile)
#                    
#                    categories2 = reflexion2.categories.names()
#            
#                    self.assertEqual(list(categories2),list(categories2))
#        
#        
#    def test_accessor_create_reflexion_already_in_db(self):
#        """
#        exemple de test qui remonte l'exception : assertEqual
#        Vérification que l'enregistrement demandé a bien été créé
#        """    
#
#        username = u"anonymous"
#        rcinfos, profile = model.accessor_get_profile(username=username)
#        if rcinfos.success:
#            text = u"idee #1"
#            rcinfos, idea = model.accessor_get_idea(text)
#            if rcinfos.success:
#                rcinfos, reflexion = model.accessor_get_reflexion(idea, profile)
#                if rcinfos.success:            
#                    rcinfos, reflexion2 = model.accessor_create_reflexion(idea, profile)
#                    if rcinfos.success:     
#                        success2, code2, message2 = generic.returned_code_ok_already_in_db()
#                        self.assertEqual(str(rcinfos.message), str(message2))     
#
#    
#    def test_reflexion_state_default_immature(self):
#        username = u"anonymous"
#        rcinfos, profile = model.accessor_get_profile(username=username)
#        if rcinfos.success:
#            text=u"idee #1"
#            rcinfos, idea = model.accessor_get_idea(text)
#            if rcinfos.success:
#                rcinfos, reflexion = model.accessor_get_reflexion(idea, profile)
#                if rcinfos.success: 
#                    rcinfos, reflexion_state = model.accessor_get_reflexionstate(reflexion)
#                    if rcinfos.success: 
#                        self.assertEqual(reflexion_state[0].state, "immature")
#                        
################################################################################    
#class AccessorPublicationTests(TestCase):        
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#        # Création d'une réflexion
#        #   association Profil «Anonymous» - Idea «u"idée #1"»
#
#        username = u"orion"
#        rcinfos, user = model.accessor_create_user(username=username)
#        if rcinfos.success:
#            text = u"idee #1"
#            tags = u"tag #1"
#            rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#            if rcinfos.success:
#                rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile) 
#                
#        username = u"anonymous"
#        rcinfos, user = model.accessor_create_user(username=username)
#        if rcinfos.success:
#            text = u"idee #2"
#            tags = u"tag #2"
#            rcinfos, idea = model.accessor_create_idea(text, tags = tags)
#            if rcinfos.success:
#                rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile) 
#      
#    def test_create_publication_without_source(self):
#        
#        username = u"orion"
#        rcinfos, profile = model.accessor_get_profile(username=username)
#        if rcinfos.success:
#            text=u"idee #1"
#            rcinfos, idea = model.accessor_get_idea(text)
#            if rcinfos.success:
#                rcinfos, reflexion = model.accessor_get_reflexion(idea, profile)
#                if rcinfos.success: 
#                    rcinfos, publication = model.accessor_create_publication(reflexion)
#                    if rcinfos.success:
#                        days = 1
#                        rcinfos, published_reflexions = model.accessor_get_newly_published_reflexions(days)
#                        if rcinfos.success:
#                            self.assertEqual(len(published_reflexions), 1)
#                            post = published_reflexions[0] 
#                            self.assertFalse(post.linked_posts.all())  
#                            
#    def test_create_publication_with_source(self):
#        
#        # création de la source
#        
#        username = u"orion"
#        rcinfos, profile = model.accessor_get_profile(username=username)
#        if rcinfos.success:
#            text=u"idee #1"
#            rcinfos, idea = model.accessor_get_idea(text)
#            if rcinfos.success:
#                rcinfos, reflexion1 = model.accessor_get_reflexion(idea, profile)
#                if rcinfos.success: 
#                    rcinfos, source = model.accessor_create_publication(reflexion1)
#                    if rcinfos.success:
#
#        # création de la réponse
#                        
#                        username = u"anonymous"
#                        rcinfos, profile = model.accessor_get_profile(username=username)
#                        if rcinfos.success:
#                            text=u"idee #2"
#                            rcinfos, idea = model.accessor_get_idea(text)
#                            if rcinfos.success:
#                                rcinfos, reflexion2 = model.accessor_get_reflexion(idea, profile)
#                                if rcinfos.success: 
#                                    rcinfos, publication = model.accessor_create_publication(reflexion2, source = source)
#                                    if rcinfos.success:
#                                        days = 1
#                                        rcinfos, published_reflexions = model.accessor_get_newly_published_reflexions(days)
#                                        if rcinfos.success:
#                                            self.assertEqual(len(published_reflexions), 2)
#                                            
#                                            post1 = published_reflexions[0] 
#                                            self.assertTrue(post1.linked_posts.all())  
#
#                                            post2 = published_reflexions[1] 
#                                            self.assertTrue(post2.linked_posts.all())  
#
#                     
################################################################################                    
#class GenericTests(TestCase):
#    
#    
#    def setUp(self):
#        """
#        Crée l'environnement des tests
#        Le setUp est appelé avant chaque méthode
#           conséquence : comme on ne veut créer les enregistrements qu'une 
#                         seule fois, il faut utiliser la méthode
#                         get_or_create pour éviter l'exception IntegrityError
#                         de la méthode create
#        """
#        user, iscreation = User.objects.get_or_create(username=u"anonymous")
#        
#        # Création d'une idée
#        text = u"idee #1"
#        idea, iscreation = model.Idea.objects.get_or_create(text=text)
#
#    def test_manage_save_exception_integrityerror(self):
#        """
#        exemple de test qui remonte l'exception : IntegrityError
#        (Demande de création d'un enregistrement qui existe déjà dans la table 
#        Idea)
#        
#        """    
#
#        user = model.User()
#        user.username = u"anonymous"
#        
#        rinfos = generic.manage_save_exception(u"test_manage_save_exception_integrityerror", user)
#
#        self.assertEqual(rinfos.code, "IntegrityError")
#        
#    def test_manage_save_exception_dataerror(self):
#        """
#        exemple de test qui remonte l'exception : DataError
#        (Demande de création d'un enregistrement avec une taille pour username
#        qui excède les 30 caractères autorisés)
#        
#        """    
#        user = model.User()
#        user.username = "anonymous"*4
#        
#        rinfos = generic.manage_save_exception(u"test_manage_save_exception_dataerror", user)
#        
#        self.assertEqual(rinfos.code, "DataError")
#        
#    def test_manage_save_exception_integrityerror_on_idea(self):
#        """
#        exemple de test qui remonte l'exception : IntegrityError
#        (Demande de création d'un enregistrement qui existe déjà dans la table 
#        Idea)
#        
#        """    
#        idea = model.Idea()
#        text = u"idee #1"
#        idea.text = text
#        
#        rinfos = generic.manage_save_exception(u"test_manage_save_exception_integrityerror_on_idea", idea)
#
#        self.assertEqual(rinfos.code, "IntegrityError")
#      
#      
################################################################################                    
class CommunicationTests(TestCase):
    
    
    def test_sort_ideas_according_request(self):
        
        username = u"orion"
        rcinfos, user_o = model.accessor_create_user(username=username)
        
        if rcinfos.success:
            
            username = u"freeroot"
            rcinfos, user_f = model.accessor_create_user(username=username)
            
            if rcinfos.success:
            
                if rcinfos.success:
                            
                    text = u"s'épaississait, parfumé par un encensoir invisible que balançaient des séraphins"
                    tags = u"tag #1"
                    tags = generic.tagscorrection(tags)
                    rcinfos, idea = model.accessor_create_idea(text, tags = tags)
        
                    if rcinfos.success:            
                        rcinfos, reflexion = model.accessor_create_reflexion(idea, user_f.profile)
                        
                            
                if rcinfos.success:
                     
                    text = u"faiblissait, parfumé par un vent qui semblait à un alizé parfumé."
                    tags = u"tag #1"
                    tags = generic.tagscorrection(tags)
                    rcinfos, idea = model.accessor_create_idea(text, tags = tags)
            
                    if rcinfos.success:            
                        rcinfos, reflexion = model.accessor_create_reflexion(idea, user_f.profile)
            
                if rcinfos.success:
                     
        #                text = u"faiblissait, parfumé par un vent qui semblait à un chagrin parfumé."
                    text = u"faiblissait, parfumé par un vent qui semblait à un alizé parfumé du désert."
                    tags = u"tag #1"
                    tags = generic.tagscorrection(tags)
                    rcinfos, idea = model.accessor_create_idea(text, tags = tags)
            
                    if rcinfos.success:            
                        rcinfos, reflexion = model.accessor_create_reflexion(idea, user_o.profile)
                        
                if rcinfos.success:
        
                    words = [u"parfumé", u"alizé"] # liste requise
                    ideas = com.IdeasContainer(words = words, 
                                               is_test = True)
                    sorted_ideas = ideas.get_context_data()
                    
                    
                    
                    requested_users = [u"freeroot"] # liste requise
                    ideas = com.IdeasContainer(words = words, 
                                               requested_users = requested_users,
                                               is_test = True)
                    sorted_ideas = ideas.get_context_data()
                    
                    excluded_users = [u"freeroot"]
                    ideas = com.IdeasContainer(words = words, 
                                               excluded_users = excluded_users,
                                               is_test = True)
                    sorted_ideas = ideas.get_context_data()
        
        
        
        
    def test_ideas_filter_com(self):
        
        username = u"orion"
        rcinfos, user = model.accessor_create_user(username=username)
        if rcinfos.success:
                    
            text = u"idee #3"
            tags = u"tag #3"
            rcinfos, idea = model.accessor_create_idea(text, tags = tags)

            if rcinfos.success:
                
                categories = 'categorie #3'   
                rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile, categories = categories)
                    
                    
#        username = u"freeroot"
#        rcinfos, user = model.accessor_create_user(username=username)
        if rcinfos.success:
            
            text = u"idee #2"
            tags = u"tag #2"
            rcinfos, idea = model.accessor_create_idea(text, tags = tags)
            
            if rcinfos.success:
  
                categories = 'categorie #2'   
                rcinfos, reflexion = model.accessor_create_reflexion(idea, user.profile, categories = categories)
                    
        if rcinfos.success:                     
        
            ideas = com.IdeasContainer()
            username_choices = ideas.get_username_choices()
            
            print list(username_choices)
            
            
###############################################################################                    
#class ProcessesTests(TestCase):
#    
#    def test_get_word_position_instances(self):
#        
#        print "test_get_word_position_instances"
#           
#        word = "le"
#        text = "s’épaississait, parfumé par un encensoir invisible que balançaient des séraphins"
#        instances = process.get_word_position_instances(word, text)
#        print instances
        
#############################################################################
