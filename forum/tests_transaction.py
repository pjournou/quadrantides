# -*- coding: utf-8 -*-
"""
Created on Fri Oct 31 19:55:45 2014

@author: Patrice
"""

# -*- coding: utf-8 -*-

###############################################################################

from django.test import TestCase, RequestFactory
from django.test import Client
#from django.contrib.auth.models import User, AnonymousUser
from django.core.urlresolvers import reverse

#import django.db as db

import forum.models as model
import forum.tasks as task
import forum.processes as process
import forum.views as view

import quadrantides.generics as generic

#############################################################################
class TaskTests(TestCase):
    
    def setUp(self):
        """
        Crée l'environnement des tests
        Le setUp est appelé avant chaque méthode
           conséquence : comme on ne veut créer les enregistrements qu'une 
                         seule fois, il faut utiliser la méthode
                         get_or_create pour éviter l'exception IntegrityError
                         de la méthode create
        """

    def test_publish_idea_in_new_thread_ok(self):

        username=u"orion"
        rcinfos, user = model.accessor_create_user(username=username)
        if rcinfos.success:
            profile = user.profile
            text=u"idee #1"
            tags = u"tag #1"
            
            categories = u"category #1"   
            tags = generic.tagscorrection(tags)  
            categories = generic.tagscorrection(categories)  


            rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                             profile, 
                                                             tags = tags, 
                                                             categories = categories)
            
            if rcinfos.success:
                days = 1
                rcinfos, published_reflexions = model.accessor_get_newly_published_reflexions(days)
                if rcinfos.success:
                    self.assertEqual(len(published_reflexions), 1)
                    post = published_reflexions[0] 
                    
                    post_profile = post.get_publication_profile()
                    self.assertEqual(post_profile.user.username, username)  
                    self.assertEqual(post.get_all_text(), text)  
                    
                    names = post.get_tag_names()
                    strnames=[]
                    for name in names:
                        strnames.append(str(name))
                    self.assertEqual(tags, strnames)  
                    
                    names = post.get_category_names()
                    strnames=[]
                    for name in names:
                        strnames.append(str(name))
                        
                    self.assertEqual(categories, strnames) 
                    
                    
    def test_publish_idea_in_new_thread_ko_rollback(self):
        
        rcinfos, discussions = model.accessor_get_all_discussions()
        self.assertFalse(rcinfos.success)
        success_ko_not_in_db, code_ko_not_in_db, message_ko_not_in_db = generic.returned_code_ko_not_in_db()
        self.assertEqual(rcinfos.message, message_ko_not_in_db)
                 
        username=u"orion"
        rcinfos, user = model.accessor_create_user(username=username)
        if rcinfos.success:
            profile = user.profile
            text=u"idee #1"
            tags = u"tag #1"
            
            categories = u"category #1"   
            tags = generic.tagscorrection(tags)  
            categories = generic.tagscorrection(categories)  

            rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                             profile, 
                                                             tags = tags, 
                                                             categories = categories,
                                                             integrity_error_simulated_failure = True)
                                                             
            success_task_aborted, code_task_aborted, message_task_aborted = generic.returned_code_ko_task_aborted()
            
            self.assertFalse(rcinfos.success)
#            self.assertEqual(rcinfos.message, message_task_aborted)
            
            
            days = 1
            rcinfos, published_reflexions = model.accessor_get_newly_published_reflexions(days)
            self.assertFalse(rcinfos.success)

            self.assertEqual(rcinfos.message, message_ko_not_in_db)
                        
            rcinfos, discussions = model.accessor_get_all_discussions()
            self.assertFalse(rcinfos.success)        
            self.assertEqual(rcinfos.message, message_ko_not_in_db)
                    

    def test_publish_idea_in_existing_thread_ok(self):
        
        # on crée la publication source
        
        username=u"freeroot"
        rcinfos, user = model.accessor_create_user(username=username)
        if rcinfos.success:
            profile = user.profile
            text=u"freeroot idee #1"
            tags = u"freeroot tag #1"
            categories = u"freeroot category #1"   
            
            tags = generic.tagscorrection(tags)  
            categories = generic.tagscorrection(categories)  

            rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                             profile, 
                                                             tags = tags, 
                                                             categories = categories)
                                                             
        
            if rcinfos.success:                
                source = discussion.publication
                
                # on crée la publication réponse
                
                username=u"orion"
                rcinfos, user = model.accessor_create_user(username=username)
                if rcinfos.success:
                    profile = user.profile
                    text=u"idee #1"
                    tags = u"tag #1"
                    
                    categories = u"category #1"   
                    tags = generic.tagscorrection(tags)  
                    categories = generic.tagscorrection(categories)  
        
                    rcinfos, discussion = task.publish_idea_in_existing_thread(text, 
                                                                         profile, 
                                                                         tags = tags, 
                                                                         categories = categories,
                                                                         source = source)
                    
                    if rcinfos.success:
                        days = 1
                        rcinfos, published_reflexions = model.accessor_get_newly_published_reflexions(days)
                        # publications ordered by "-date"
                        if rcinfos.success:
                            self.assertEqual(len(published_reflexions), 2)
                            response = published_reflexions[0] 
                            self.assertTrue(process.has_source(response))
                            source2 = published_reflexions[1] 
                            self.assertTrue(process.has_responses(source2))
                            self.assertEqual(source, source2)
                            
                            
                            
    def test_publish_idea_in_existing_thread_ko_rollback(self):
        
        # on crée la publication source
        
        username=u"freeroot"
        rcinfos, user = model.accessor_create_user(username=username)
        if rcinfos.success:
            profile = user.profile
            text=u"freeroot idee #1"
            tags = u"freeroot tag #1"
            categories = u"freeroot category #1"   
            
            tags = generic.tagscorrection(tags)  
            categories = generic.tagscorrection(categories)  

            rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                             profile, 
                                                             tags = tags, 
                                                             categories = categories)
                                                             
        
            if rcinfos.success:                
                source = discussion.publication
                
                # on crée la publication réponse
                
                username=u"orion"
                rcinfos, user = model.accessor_create_user(username=username)
                if rcinfos.success:
                    profile = user.profile
                    text=u"idee #1"
                    tags = u"tag #1"
                    
                    categories = u"category #1"   
                    tags = generic.tagscorrection(tags)  
                    categories = generic.tagscorrection(categories)  
        
                    rcinfos, discussion = task.publish_idea_in_existing_thread(text, 
                                                                         profile, 
                                                                         tags = tags, 
                                                                         categories = categories,
                                                                         source = source,
                                                                         integrity_error_simulated_failure = True)
                    
                    self.assertFalse(rcinfos.success)
                    
                    days = 1
                    rcinfos, published_reflexions = model.accessor_get_newly_published_reflexions(days)
                    
                    # publications ordered by "-date"
                    if rcinfos.success:
                        # Il s'agit de la source, la réponse n'existant pas (rollback)
                        self.assertEqual(len(published_reflexions), 1)
                        source = published_reflexions[0] 
                        # La preuve est faite : elle n'est pas une source
                        # et ne contient pas de réponse
                        self.assertFalse(process.has_source(source))
                        self.assertFalse(process.has_responses(source))

                                       
#############################################################################
class urlTests(TestCase):
    
    def setUp(self):
        """
        Crée l'environnement des tests
        Le setUp est appelé avant chaque méthode
           conséquence : comme on ne veut créer les enregistrements qu'une 
                         seule fois, il faut utiliser la méthode
                         get_or_create pour éviter l'exception IntegrityError
                         de la méthode create
        """
        self.factory = RequestFactory()

        username = "anonymous"
        password = "secret"
        rcinfos, user = model.accessor_create_user(username=username, password=password)
        
        username = "orion"
        password = "secret"
        rcinfos, user = model.accessor_create_user(username=username, password=password)
        self.user = user

    def test_home(self):
        # Create an instance of a GET request.
        request = self.factory.get('/forum/accueil')

        # Recall that middleware are not supported. You can simulate a
        # logged-in user by setting request.user manually.
        request.user = self.user

        response = view.home(request)
        self.assertEqual(response.status_code, 200)
        
    def test_newly_published_reflexions_with_no_content(self):
        client = Client()
        response = client.get(reverse('forum_last_news'))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['recent_posts'])
        
#        # Create an instance of a GET request.
#        request = self.factory.get('/dernieres/actualites')
#
#        # Recall that middleware are not supported. You can simulate a
#        # logged-in user by setting request.user manually.
#        request.user = self.user
#
##        # Or you can simulate an anonymous user by setting request.user to
##        # an AnonymousUser instance.
##        request.user = AnonymousUser()
#
#        # Test my_view() as if it were deployed at /customer/details
#        response = view.NewlyPublishedReflexions.as_view()
#        self.assertNotEqual(response.status_code, 200)
        
        
    def test_newly_published_reflexions_with_one_content(self):

        username=u"orion"
        rcinfos, profile = model.accessor_get_profile(username=username)
        self.assertTrue(rcinfos.success)
        if rcinfos.success:
            text=u"idee #1"
            tags = u"tag #1"
            
            categories = u"category #1"   
            tags = generic.tagscorrection(tags)  
            categories = generic.tagscorrection(categories)  

            rcinfos, discussion = task.publish_idea_in_new_thread(text, 
                                                             profile, 
                                                             tags = tags, 
                                                             categories = categories)
            self.assertTrue(rcinfos.success)
            if rcinfos.success:

                client = Client()
                response = client.get(reverse('forum_last_news'))

                self.assertTrue(response.context['recent_posts'])
                lpost= generic.returned_var_as_list(discussion.publication)
#                print type(lpost)
                post_context_list = process.get_post_context_list(lpost)
                post_context = post_context_list[0]
                
#                print post_context.post
#                print post_context.sources_count
#                print post_context.responses_count   
                
                
                context = response.context['recent_posts']

                self.assertEqual(len(post_context_list), len(context))          
                self.assertEqual(post_context.post, context[0].post)  
                self.assertEqual(post_context.sources_count, context[0].sources_count)                  
                self.assertEqual(post_context.responses_count, context[0].responses_count)    
                
        
#    def test_login(self):
#        
#        client = Client()
#        login = client.login(username=self.user.username, password=self.user.password)
##        response = client.post('/connexion/', {'username': 'orion2', 'password': 'orion'})
#        response = client.get('/forum/accueil/')
##        print response.content
#        self.assertContains(response, "Soyons philosophes")
        # Réponse 200 OK.
#        self.assertTrue(login)
        
#        print response.status_code
#        response = c.get('/customer/details/')