# -*- coding: utf-8 -*-
"""
Created on Thu Nov 27 19:56:45 2014

@author: Patrice
"""
###############################################################################

import forum.models as model
import forum.processes as process
import quadrantides.generics as generic
import logging


###############################################################################
class ReflexionCom(object):
  
    def __init__(self, index, state):
        
        self.index = index
        self.state = state
        self.actions=[u"", u"évolution", u"modify_category"]
        self.requested_action_index = 0
        
    def set_requested_action_index(self, requested_action_index):
        self.requested_action_index = requested_action_index
        
    def get_label_prefix(self):
        title = u"Réflexion « " 
        return u"%s" % title
        
    def get_label_suffix(self):
        title = u" » depuis le {0}".format(self.state.date.strftime(u"%d %B %Y  %H:%M:%S"))
        return u"%s" % title
        
###############################################################################  
class ReflexionsPerProfileCom(object):
  
    def __init__(self, profile):
         
        rcinfos, states = model.accessor_get_latest_profile_reflexion_states(profile)
        self.data = []
        for index in range(len(states)):
            self.data.append(ReflexionCom(index, states[index]))
            
    def get_context_data(self):
        return self.data
        
    def get_mature_state_context_data(self):
        
        mature_state_context_data = []
        for data in self.data:
            logging.info('ReflexionsPerProfileCom/get_mature_state_context_data/valeur de data.state.state.count("mature") : %s' % data.state.state.count("mature"))
            if data.state.state == "mature":
                logging.info('ReflexionsPerProfileCom/get_mature_state_context_data/valeur de data (appended) : %s' % data)
                mature_state_context_data.append(data)
        return mature_state_context_data
        
        
    def get_without_category_context_data(self):
        
        context_data = []
        for data in self.data:
            if len(data.state.reflexion.categories.names()) == 0:
                context_data.append(data)
        return context_data
        
        
    def set_requested_action_index(self, index, requested_action_index):
        data = self.get_context_data()
        logging.info('ReflexionsPerProfileCom/set_requested_action_index/valeur de index : %s' % type(index))
        for wdata in data:
            logging.info('ReflexionsPerProfileCom/set_requested_action_index/valeur de wdata.index : %s' % type(wdata.index))
            
        boolarray = [wdata.index == index for wdata in data]
        logging.info('ReflexionsPerProfileCom/set_requested_action_index/valeur de boolarray : %s' % boolarray)
        index = boolarray.index(True)
        
        data[index].set_requested_action_index(requested_action_index)
        
        
############################################################################### 
class ReflexionsPerProfilePerCategoryCom(object):
  
    def __init__(self, profile, slug):
         
        rcinfos, states = model.accessor_get_latest_category_slug_reflexion_states(profile, slug)
        
        self.data = []
        for index in range(len(states)):
            self.data.append(ReflexionCom(index, states[index]))
            
    def get_context_data(self):
        return self.data
        
    def set_requested_action_index(self, index, requested_action_index):
        data = self.get_context_data()
        logging.info('ReflexionsCom/set_requested_action_index/valeur de index : %s' % type(index))
        for wdata in data:
            logging.info('ReflexionsCom/set_requested_action_index/valeur de wdata.index : %s' % type(wdata.index))
            
        boolarray = [wdata.index == index for wdata in data]
        logging.info('ReflexionsCom/set_requested_action_index/valeur de boolarray : %s' % boolarray)
        index = boolarray.index(True)
        
        data[index].set_requested_action_index(requested_action_index)
        

###############################################################################
class Idea(generic.Debugger):
  
    def __init__(self, idea, profiles, **kwargs):
        
        self.idea = idea
        self.profiles = profiles
        self.has_words = False

        super(Idea, self).__init__(**kwargs)

        if kwargs:
            if 'words' in kwargs:
                self.has_words = True
                words = kwargs['words']   
        
                # recherche des mots contenus dans les idées
        
                text = idea.text.lower()            
                boolarray = [word in text for word in words] 
        
                # on contruit une liste de tuples pour les mots trouvés
                # (mot, position)
        
                woi = []
                for word in words:
                    instances = process.get_word_position_instances(word, text)
                    if instances:
                        for instance in instances:
                            woi.append(instance)
               
                logging.info('Idea/__init__/valeur de woi : %s' % woi)
                
                posarray = [text.find(" " + word.lower()) for word in words] 
                
                
                self.woi = woi
                
                logging.info('Idea/__init__/valeur de words : %s' % words)
                logging.info('Idea/__init__/valeur de boolarray : %s' % boolarray)
                logging.info('Idea/__init__/valeur de posarray : %s' % posarray)                

                found_words = [w[0] for w in woi] 
                
                uniq_found_words = set(found_words)
                self.how_many_words = len(uniq_found_words) #boolarray.count(True)
        
                found_words = [value for index,value in enumerate(words) if boolarray[index]]
                
                self.found_words = found_words
        
                    
                # recherche des mots correspondant à un utilisateur
                logging.info('Idea/__init__/valeur de self.how_many_words : %s' % self.how_many_words)
                logging.info('Idea/__init__/valeur de words : %s' % words)
                logging.info('Idea/__init__/valeur de found_words : %s' % self.found_words)

                if self.is_debugger_active_for_test():
                    print "------------------------------------------------------------------------"
                    print u"  CLASSE   : communications.Idea"
                    print u"  FONCTION : __init__   "
                    
                    print u"  RAPPORT  : n°1"
                    print u""
                    print u"     La requête utilisateur recouvre %s occurence(s) commune(s) avec le texte " % self.how_many_words
                    print u"     de l'idée en cours d'examen"
                    print u"     - Les mots recherchés étaient : %s " % words
                    print u"     - Le texte de l'idée était : %s " % idea.text

                        
#                how_many_requested_users = 0
#
#                for word in words: 
#                    for profile in profiles:
#                        if word.lower() == profile.user.username.lower():
#                            how_many_requested_users+=1
#                        
#                self.how_many_users = how_many_requested_users
#                logging.info('Idea/__init__/valeur de how_many_requested_users : %s' % how_many_requested_users)
        
#        self.actions=[u"", u"Ajouter à la composition"]
#        self.requested_action_index = 0
        
    def has_words(self):
        return self.has_words
        
    def set_requested_action_index(self, requested_action_index):
        self.requested_action_index = requested_action_index
   
    def get_usernames(self):
        
        usernames = []

        for profile in self.profiles:
            usernames.append(profile.user.username)  
        return usernames
        
    def get_usernames_for_display(self):
        title = ""
        i = 0
        n = len(self.profiles)
        for profile in self.profiles:
            text = profile.user.username
            title += text
            if i > 0 & i < n:
                title = title + u"\n"
            i += 1
            
        return title
        
###############################################################################
class IdeasContainer(generic.Debugger):
 
    def __init__(self, **kwargs):
        
        super(IdeasContainer, self).__init__(**kwargs)

        self.has_words = False
        self.has_requested_users = False
        self.has_excluded_users = False
        
        if kwargs:
            if 'words' in kwargs:
        
                self.words = kwargs['words']
                self.has_words = True
               
            if 'requested_users' in kwargs:
        
                self.requested_users = kwargs['requested_users']
                logging.info('IdeasContainer/__init__/self.requested_users : <%s>' % self.requested_users)
                if self.requested_users == [u""]:
                    pass
                else:
                    self.has_requested_users = True
                
            if 'excluded_users' in kwargs:
        
                self.excluded_users = kwargs['excluded_users']
                if self.excluded_users == [u""]:
                    pass
                else:
                    self.has_excluded_users = True
                
                
      
        self.data = []

        rcinfos, ideas = model.accessor_get_ideas()  
    
        if rcinfos.success:
            for idea in ideas:
                rcinfos, reflexions = model.accessor_get_reflexions(idea)
                if rcinfos.success:
                    profiles = []
                    for reflexion in reflexions:
                        profiles.append(reflexion.profile)
                    self.data.append(Idea(idea, profiles, **kwargs))
          
        
                
    def sort_ideas_according_number_of_words(self, ideas):
        
        how_many_words = []
        
        for idea in ideas:
            how_many_words.append(idea.how_many_words)

        s_tuple_how_many_words = sorted(enumerate(how_many_words), key=lambda x: x[1], reverse = True)
        
        s_how_many_words = [ t[1] for t in s_tuple_how_many_words]
        sorted_indexes = [ t[0] for t in s_tuple_how_many_words]

            
        if self.is_debugger_active_for_test():
            print "------------------------------------------------------------------------"
            print u"  CLASSE   : communications.IdeasContainer"
            print u"  FONCTION : sort_ideas_according_number_of_words"
            print u"" 
            print "------------------------------------------------------------------------"
            print u"  RAPPORT  : INITIALISATION"
            print u"    n°1         "
            print "------------------------------------------------------------------------"
            print u"             Tableau des nombres de mots trouvés pour chacune des réflexions"            
            print u"             indices : {0}".format(range(len(sorted_indexes)))
            print u"             valeurs nombre de mots : {0}".format(how_many_words)  
            print u""            
            
            print "------------------------------------------------------------------------"
            print u"  RAPPORT  : TRI DECROISSANT sur les occurences de mots"
            print u"    n°2        "
            print "------------------------------------------------------------------------"
            print u"             Tableau trié des nombres de mots calculés pour chacune des réflexions"            
            print u"               indices : {0}".format(sorted_indexes)
            print u"               valeurs nombre de mots : {0}".format(s_how_many_words)

        logging.info('IdeasContainer/get_context_data/après le tri, les textes sont rangés dans cet ordre : %s' % sorted_indexes)   
        
        return sorted_indexes
        
    def sort_ideas_according_request(self):
        
        # 1 / on exclut les réflexions de certains utilisateurs (self.exclude_users)
        
        if self.has_excluded_users:
            
            extracted_ideas = []
            
            for idea in self.data:
                for profile in idea.profiles:
                    if profile.user.username in self.excluded_users:
                        pass
                    else:
                        extracted_ideas.append(idea)
                        
            ideas = extracted_ideas
            
        else:
            ideas = self.data


        # 2 / on ne garde que les réflexions de certains utilisateurs (self.include_users)
        logging.info('IdeasContainer/sort_ideas_according_request/has_requested_users : %s' % self.has_requested_users)
        if self.has_requested_users:
            
            extracted_ideas = []
            
            for idea in ideas:
                for profile in idea.profiles:
                    if profile.user.username in self.requested_users:
                        extracted_ideas.append(idea)
                        
            ideas = extracted_ideas


        # 3 / Réarrangement des réflexions restantes :
        # - sur le nombre d'occurences trouvées pour chaque mot 
        #   présent dans la requête de recherche
        #   TRI DECROISSANT

        if len(ideas) > 0:
            
            sorted_indexes = self.sort_ideas_according_number_of_words(ideas)
            sorted_ideas = [ideas[sorted_index] for sorted_index in sorted_indexes]
        else:
            sorted_ideas = []
        
        
        return sorted_ideas
        
    
    def get_context_data(self):
        
        if self.has_words or self.has_requested_users or self.has_excluded_users:
            
            data = self.sort_ideas_according_request()
            
        else:
            data = self.data
        
        rcinfos = generic.get_returned_code_ok_object()
        return rcinfos, data

    
    def get_username_choices(self):
        usernames = [u"Toutes"]
        for data in self.data:
            curusernames = data.get_usernames()
            for curusername in curusernames:
                usernames.append(curusername)
        usernames = list(set(usernames))
        usernames = sorted(usernames)

        
        username_choices = []
        i = 0
        for username in usernames:
            username_choices.append((i, username))
            i+=1
        return username_choices
        
        
    def set_requested_action_index(self, index, requested_action_index):
        data = self.get_context_data()
            
        boolarray = [wdata.index == index for wdata in data]
        logging.info('IdeasFilterCom/set_requested_action_index/valeur de boolarray : %s' % boolarray)
        index = boolarray.index(True)
        
        data[index].set_requested_action_index(requested_action_index)
        