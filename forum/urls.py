#-*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from forum.views import NewlyPublishedReflexions, PostDetail, ThreadsList
from forum.views import AccountCreationView

urlpatterns = patterns('forum.views',
###############################################################################
# accueil
	url(r'^accueil/$', 'home', name="home"), 
###############################################################################
# réflexions
     url(r'^reflexion/nouvelle/simple/$', 'create_simple_reflexion_form', name='simple_reflexion_form'),
     url(r'^reflexion/nouvelle/composee/$', 'create_compounded_reflexion_form', name='compounded_reflexion_form'),
#	url(r'^reflexion/nouvelle/composee/action/(?P<action>[-\w]+)/$', 
#         'create_compounded_reflexion_form', 
#         name="compounded_reflexion_form"), 

###############################################################################
# actualités
	url(r'^dernieres/actualites/$', NewlyPublishedReflexions.as_view(), name="forum_last_news"), 
###############################################################################
# affichage du détail d'une publication


    url(r'^consultation/publication/(?P<pk>\d+)$', PostDetail.as_view(), name="forum_post_detail"),
    url(r'^reponse/publication/(?P<id>\d+)/detail_source/(?P<afficher_source>\d+)$', 
        'create_publication_form',
        name = "forum_publication_response"),
###############################################################################
# discussions

    url(r'^discussion/nouvelle/$', 'create_thread_form', name='create_thread_form'),
	url(r'^discussions/accueil$', 'forum_threads_overview', name="forum_threads_overview"), 
	url(r'^discussions/$', ThreadsList.as_view(), name="forum_threads_list"), 
###############################################################################
# création de comptes utilisateurs
 
	url(r'^creer/compte/utilisateur/$', AccountCreationView.as_view(), name="create_account"), 
	url(r'^creer/compte/utilisateur/succes$', 'create_account_success'),
###############################################################################
# espace personnel : subjectivité privée
	url(r'^acces/espace_personnel/accueil$', 'personal_space_overview', name="forum_personal_space_overview"),  

	url(r'^acces/espace_personnel$', 'personal_space_display', name="forum_personal_space"), 
	url(r'^acces/espace_personnel/nouveau/theme$', 'personal_space_new_theme', name="forum_personal_new_theme"), 
	url(r'^acces/espace_personnel/index/(?P<index>\d+)/action/(?P<action>[-\w]+)/$', 
         'personal_space_display_with_reflexion_state_form', 
         name="personal_space_display_with_reflexion_state_form"), 
    url(r'^acces/espace_personnel/categorie/(?P<slug>[-\w]+)/$', 'personal_space_display_by_category', name='category_tagged'),
###############################################################################
# l'espace personnel des réflexions mature : subjectivité publique
	url(r'^acces/vue/generale/subjectivite$', 'subjectivity_overview_display', name="forum_subjectivity_overview"), 
	url(r'^acces/subjectivite/(?P<index>\d+)/$', 
         'mature_reflexions_display', 
         name="forum_mature_reflexions_display"),   
        
)
