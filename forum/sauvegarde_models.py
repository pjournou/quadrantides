#-*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class Tag(models.Model):
    nom = models.CharField(max_length=20)
    description = models.TextField(null=True)
    def __unicode__(self):
        """ 
        Cette méthode que nous définirons dans tous les modèles
        nous permettra de reconnaître facilement les différents objets que nous
        traiterons plus tard et dans l'administration
        """
	return u"%s" % self.nom
        
    
class Personne(models.Model):
    prenom = models.CharField(max_length=30)
    nom = models.CharField(max_length=30)
    email = models.EmailField(null=True)
    date_creation = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date création")
    
    class Meta:
        abstract = True  
    def get_name(self):
        return self.nom
  
class Utilisateur(Personne):
    pseudo = models.CharField(max_length=10, verbose_name="pseudo", primary_key=True)
    motdepasse = models.CharField(max_length=15, verbose_name="mot de passe", null=False)
    
    roles_possibles = (
        ('moderateur', 'modérateur'),
        ('participant', 'participant'),
        )
    role = models.CharField(max_length=11, 
                            verbose_name="rôle",
                            choices = roles_possibles,
                            default = 'participant')
    presentsurlesite = models.BooleanField(default = False, verbose_name="si coché, indique que l'Utilisateur se trouve connecté sur le site en ce moment")
    date_connexion = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name="dernière connexion le")
    
 #   publications = models.ManyToManyField(Reflexion, through = 'CommunicationPublique')
    def get_pseudo(self):
        return self.pseudo
        
    def __unicode__(self):
        """ 
        Cette méthode que nous définirons dans tous les modèles
        nous permettra de reconnaître facilement les différents objets que nous
        traiterons plus tard et dans l'administration
        """
	return u"%s" % self.pseudo

class Moderation(models.Model):
    moderateur = models.ForeignKey('Utilisateur')
    date_creation = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date création")	
    autorisee = models.BooleanField(default = False, verbose_name="si autorisée, cochez")
    raison = models.TextField(null=False)
    def __unicode__(self):
        """ 
        Cette méthode que nous définirons dans tous les modèles
        nous permettra de reconnaître facilement les différents objets que nous
        traiterons plus tard et dans l'administration
        """
	return u"modérateur : {0} - raison : {1}".format(self.moderateur.get_pseudo(), self.raison[0:70])
    #return u"%s" % self.raison[0:70]
    
# class CibleModeration(models.Model):
    # reflexions = models.ManyToManyField(Reflexion) 
    # def __unicode__(self):
        # """ 
        # Cette méthode que nous définirons dans tous les modèles
        # nous permettra de reconnaître facilement les différents objets que nous
        # traiterons plus tard et dans l'administration
        # """
	# return "Table : {0} - Attribut : {1}".format(self.nomtable, self.nomattribut)
    

class Reflexion(models.Model):
    texte = models.TextField(null=False)
    date_creation = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=u"date création")
    date_autorisation = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name="date autorisation")
    tags = models.ManyToManyField(Tag) 
    moderations = models.ManyToManyField(Moderation)
    
    def __unicode__(self):
        """ 
        Cette méthode que nous définirons dans tous les modèles
        nous permettra de reconnaître facilement les différents objets que nous
        traiterons plus tard et dans l'administration
        """
	return u"%s" % self.texte[0:70]
	
    def get_reflexion_text(self):
    
        return self.texte
		
    def get_reflexion_short_text(self):
    
        texte = self.texte
        if len(self.texte) > 150:
            return '%s...' % texte
        else:
            return texte
	
	
    
class Discussion(models.Model):
    titre = models.CharField(max_length=300)
    date_ouverture = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date d'ouverture")
    date_autorisation = models.DateField(auto_now_add=False, auto_now=True, verbose_name="date MAJ moderation")
    moderations = models.ManyToManyField(Moderation)
    createur = models.ForeignKey('Utilisateur')
    def get_title(self):
        return u"%s" % self.titre	
    def __unicode__(self):
        return u"%s" % self.titre
	
class CommunicationPublique(models.Model):
	date_publication = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date de publication")
	participant = models.ForeignKey('Utilisateur')
	reflexion = models.ForeignKey('Reflexion')
	discussion = models.ForeignKey('Discussion')
	sources = models.ManyToManyField("self", null=True, blank=True, verbose_name="sources associées")
	commentaires = models.ManyToManyField("self", null=True, blank=True, verbose_name="commentaires associés")
		
	def get_title(self):

		title = str(self.date_publication) + '-' + self.participant.get_pseudo() + '-' + self.discussion.get_title()

		return u"%s" % title
		
	def get_reflexion_short_text(self):

		text = self.reflexion.get_reflexion_short_text()

		return u"%s" % text
		
	def get_reflexion_text(self):
	
		text = self.reflexion.get_reflexion_text()
		
		return u"%s" % text
		
	def __unicode__(self):
		return u"%s" % self.get_title()