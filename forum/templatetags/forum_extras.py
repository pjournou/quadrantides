# -*- coding: utf-8 -*-
"""
Created on Fri Dec 12 21:43:12 2014

@author: Patrice
"""

from django import template
import logging
from django.utils.safestring import mark_safe
import forum.processes as process

register = template.Library()

@register.filter(is_safe=True)
def put_requested_words_in_bold(context):
    wtext = context.idea.text
#    for word in context.found_words:
#        pos = wtext.find(word)
#        wtext = wtext[0:pos] + '<strong>' + wtext[pos : pos + len(word)] + '</strong>' + wtext[pos + len(word):]
    logging.info('forum_extras/put_requested_words_in_bold/valeur de context.woi : %s' % context.woi) 
    
    # on trie les positions par ordre décroissant
    # pour que l'ajout des balises ne perturbe pas les positions calculées
    
    s_woi = sorted(context.woi, key=lambda x: x[1], reverse = True)
    
    
    for instance in s_woi:
        if len(instance)==2:
            pos = instance[1]
            word_len = len(instance[0])
            wtext = wtext[0:pos] + '<strong>' + wtext[pos : pos + word_len] + '</strong>' + wtext[pos + word_len:]    
        else:
            logging.info('forum_extras/put_requested_words_in_bold/valeur de put_requested_words_in_bold : %s' % put_requested_words_in_bold)
        
    return mark_safe(wtext)


###############################################################################
@register.filter(is_safe=True)
def create_tag_names_comprehensive_communication_need(post):

    tags = post.get_tag_names()

    tag_names_comprehensive_communication_need = "Tag(s) : "
    for i in range(len(tags)):
        if i <> 0:
            tag_names_comprehensive_communication_need += ", "
        tag_names_comprehensive_communication_need += tags[i]
        

            

    return tag_names_comprehensive_communication_need
###############################################################################
@register.filter(is_safe=True)
def put_post_tags_in_bold(post):
    
    text = post.get_all_text()
    tags = post.get_tag_names()
    logging.info('forum_extras/put_post_tags_in_bold/valeur de tags : %s' % tags)
    if tags:
        # on trie les positions par ordre décroissant
        # pour que l'ajout des balises ne perturbe pas les positions calculées
        positions = []
        for tag in tags:
            instances = process.get_word_position_instances(tag, text)
            if instances:
                for instance in instances:
                    positions.append(instance)
                                
        spositions = sorted(positions, key=lambda x: x[1], reverse = True)
        
        logging.info('forum_extras/put_post_tags_in_bold/valeur de stags : %s' % spositions)
    
        for instance in spositions:
            if len(instance)==2:
                pos = instance[1]
                word_len = len(instance[0])
                text = text[0:pos] + '<mark>' + text[pos : pos + word_len] + '</mark>' + text[pos + word_len:]    

        
    return mark_safe(text)