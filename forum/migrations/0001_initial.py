# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discussion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('publication_order', models.PositiveIntegerField(verbose_name=b'ordre de publication dans le fil de discussion')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Idea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(unique=True, verbose_name=b'texte')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name=b'date cr\xc3\xa9ation')),
                ('moderation_date', models.DateTimeField(auto_now=True, verbose_name=b'date de la derni\xc3\xa8re mod\xc3\xa9ration')),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', help_text='A comma-separated list of tags.', verbose_name='Tags')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Moderation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(default=b'conforme_ethique', max_length=100, verbose_name=b'commentaire', choices=[(b'conforme_ethique', b"conforme \xc3\xa0 l'\xc3\xa9thique du site"), (b'pas_conforme_propos_injurieux', b'pas conforme: propos injurieux')])),
                ('object_id', models.PositiveIntegerField()),
                ('auteur', models.ForeignKey(verbose_name=b'auteur', to=settings.AUTH_USER_MODEL)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site_web', models.URLField(blank=True)),
                ('avatar', models.ImageField(null=True, upload_to=b'avatars/', blank=True)),
                ('signature', models.TextField(blank=True)),
                ('inscrit_newsletter', models.BooleanField(default=False)),
                ('is_logged', models.BooleanField(default=False)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PublishedReflexion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name=b'date de publication')),
                ('linked_posts', models.ManyToManyField(related_name='linked_posts_rel_+', null=True, verbose_name=b'sources, r\xc3\xa9ponses', to='forum.PublishedReflexion', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reflexion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('categories', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', help_text='A comma-separated list of tags.', verbose_name='Tags')),
                ('idea', models.ForeignKey(to='forum.Idea')),
                ('profile', models.ForeignKey(to='forum.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReflexionState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now=True, verbose_name="date de l'\xe9volution", auto_now_add=True)),
                ('state', models.CharField(default=b'mature', max_length=70, verbose_name="\xe9tat de l'\xe9volution", choices=[(b'immature', b'immature, invisible des autres Utilisateurs'), (b'mature', b'mature, visible des autres Utilisateurs'), (b'caduque', b'caduque, invisible des autres Utilisateurs')])),
                ('reflexion', models.ForeignKey(to='forum.Reflexion')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name=b"date d'ouverture")),
                ('is_opened', models.BooleanField(default=True, verbose_name=b'coch\xc3\xa9 si la discussion est actuellement ouverte')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='publishedreflexion',
            name='reflexions',
            field=models.ManyToManyField(to='forum.Reflexion', verbose_name=b'Liste des reflexions'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='discussion',
            name='publication',
            field=models.ForeignKey(to='forum.PublishedReflexion'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='discussion',
            name='thread',
            field=models.ForeignKey(to='forum.Thread'),
            preserve_default=True,
        ),
    ]
