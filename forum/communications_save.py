# -*- coding: utf-8 -*-
"""
Created on Thu Nov 27 19:56:45 2014

@author: Patrice
"""
###############################################################################

import forum.models as model
import forum.processes as process
import quadrantides.generics as generic
import logging


###############################################################################
class ReflexionCom(object):
  
    def __init__(self, index, state):
        
        self.index = index
        self.state = state
        self.actions=[u"", u"évolution"]
        self.requested_action_index = 0
        
    def set_requested_action_index(self, requested_action_index):
        self.requested_action_index = requested_action_index
        
    def get_label_prefix(self):
        title = u"Réflexion « " 
        return u"%s" % title
        
    def get_label_suffix(self):
        title = u" » depuis le {0}".format(self.state.date.strftime(u"%d %B %Y  %H:%M:%S"))
        return u"%s" % title
        
###############################################################################  
class ReflexionsPerProfileCom(object):
  
    def __init__(self, profile):
         
        rcinfos, states = model.accessor_get_latest_profile_reflexion_states(profile)
        self.data = []
        for index in range(len(states)):
            self.data.append(ReflexionCom(index, states[index]))
            
    def get_context_data(self):
        return self.data
        
    def get_mature_state_context_data(self):
        
        mature_state_context_data = []
        for data in self.data:
            logging.info('ReflexionsPerProfileCom/get_mature_state_context_data/valeur de data.state.state.count("mature") : %s' % data.state.state.count("mature"))
            if data.state.state == "mature":
                logging.info('ReflexionsPerProfileCom/get_mature_state_context_data/valeur de data (appended) : %s' % data)
                mature_state_context_data.append(data)
        return mature_state_context_data
        
    def set_requested_action_index(self, index, requested_action_index):
        data = self.get_context_data()
        logging.info('ReflexionsPerProfileCom/set_requested_action_index/valeur de index : %s' % type(index))
        for wdata in data:
            logging.info('ReflexionsPerProfileCom/set_requested_action_index/valeur de wdata.index : %s' % type(wdata.index))
            
        boolarray = [wdata.index == index for wdata in data]
        logging.info('ReflexionsPerProfileCom/set_requested_action_index/valeur de boolarray : %s' % boolarray)
        index = boolarray.index(True)
        
        data[index].set_requested_action_index(requested_action_index)
        
        
############################################################################### 
class ReflexionsPerProfilePerCategoryCom(object):
  
    def __init__(self, profile, slug):
         
        rcinfos, states = model.accessor_get_latest_category_slug_reflexion_states(profile, slug)
        
        self.data = []
        for index in range(len(states)):
            self.data.append(ReflexionCom(index, states[index]))
            
    def get_context_data(self):
        return self.data
        
    def set_requested_action_index(self, index, requested_action_index):
        data = self.get_context_data()
        logging.info('ReflexionsCom/set_requested_action_index/valeur de index : %s' % type(index))
        for wdata in data:
            logging.info('ReflexionsCom/set_requested_action_index/valeur de wdata.index : %s' % type(wdata.index))
            
        boolarray = [wdata.index == index for wdata in data]
        logging.info('ReflexionsCom/set_requested_action_index/valeur de boolarray : %s' % boolarray)
        index = boolarray.index(True)
        
        data[index].set_requested_action_index(requested_action_index)
        

###############################################################################
class Idea(generic.Debugger):
  
    def __init__(self, idea, profiles, **kwargs):
        
        self.idea = idea
        self.profiles = profiles
        self.has_words = False

        super(Idea, self).__init__(**kwargs)

        if kwargs:
            if 'words' in kwargs:
                self.has_words = True
                words = kwargs['words']   
        
                # recherche des mots contenus dans les idées
        
                text = idea.text.lower()            
                boolarray = [word in text for word in words] 
        
                # on contruit une liste de tuples pour les mots trouvés
                # (mot, position)
        
                woi = []
                for word in words:
                    instances = process.get_word_position_instances(word, text)
                    if instances:
                        for instance in instances:
                            woi.append(instance)
               
                logging.info('Idea/__init__/valeur de woi : %s' % woi)
                
                posarray = [text.find(" " + word.lower()) for word in words] 
                
                
                self.woi = woi
                
                logging.info('Idea/__init__/valeur de words : %s' % words)
                logging.info('Idea/__init__/valeur de boolarray : %s' % boolarray)
                logging.info('Idea/__init__/valeur de posarray : %s' % posarray)                

                found_words = [w[0] for w in woi] 
                
                uniq_found_words = set(found_words)
                self.how_many_words = len(uniq_found_words) #boolarray.count(True)
        
                found_words = [value for index,value in enumerate(words) if boolarray[index]]
                
                self.found_words = found_words
        
                    
                # recherche des mots correspondant à un utilisateur
                logging.info('Idea/__init__/valeur de self.how_many_words : %s' % self.how_many_words)
                logging.info('Idea/__init__/valeur de words : %s' % words)
                logging.info('Idea/__init__/valeur de found_words : %s' % self.found_words)

                if self.is_debugger_active_for_test():
                    print "------------------------------------------------------------------------"
                    print u"  CLASSE   : communications.Idea"
                    print u"  FONCTION : __init__   "
                    
                    print u"  RAPPORT  : n°1"
                    print u""
                    print u"     La requête utilisateur recouvre %s occurence(s) commune(s) avec le texte " % self.how_many_words
                    print u"     de l'idée en cours d'examen"
                    print u"     - Les mots recherchés étaient : %s " % words
                    print u"     - Le texte de l'idée était : %s " % idea.text

                        
#                how_many_requested_users = 0
#
#                for word in words: 
#                    for profile in profiles:
#                        if word.lower() == profile.user.username.lower():
#                            how_many_requested_users+=1
#                        
#                self.how_many_users = how_many_requested_users
#                logging.info('Idea/__init__/valeur de how_many_requested_users : %s' % how_many_requested_users)
        
#        self.actions=[u"", u"Ajouter à la composition"]
#        self.requested_action_index = 0
        
    def has_words(self):
        return self.has_words
        
    def set_requested_action_index(self, requested_action_index):
        self.requested_action_index = requested_action_index
   
    def get_usernames(self):
        
        usernames = []

        for profile in self.profiles:
            usernames.append(profile.user.username)  
        return usernames
        
    def get_usernames_for_display(self):
        title = ""
        i = 0
        n = len(self.profiles)
        for profile in self.profiles:
            text = profile.user.username
            title += text
            if i > 0 & i < n:
                title = title + u"\n"
            i += 1
            
        return title
        
###############################################################################
class IdeasContainer(generic.Debugger):
 
    def __init__(self, **kwargs):
        
        super(IdeasContainer, self).__init__(**kwargs)

        self.has_words = False
        
        if kwargs:
            if 'words' in kwargs:
        
                self.words = kwargs['words']
                self.has_words = True
                
            if 'include_users' in kwargs:
        
                self.include_users = kwargs['include_users']
                self.has_include_users = True
                
            if 'exclude_users' in kwargs:
        
                self.exclude_users = kwargs['exclude_users']
                self.has_exclude_users = True
                
                
      
        self.data = []

        rcinfos, ideas = model.accessor_get_ideas()  
    
        if rcinfos.success:
            for idea in ideas:
                rcinfos, reflexions = model.accessor_get_reflexions(idea)
                if rcinfos.success:
                    profiles = []
                    for reflexion in reflexions:
                        profiles.append(reflexion.profile)
                    self.data.append(Idea(idea, profiles, **kwargs))
          
        
                
    def sort_ideas_according_number_of_words(self, ideas):
        
        how_many_words = []
#        how_many_users = [] 
        
        for idea in ideas:
            how_many_words.append(idea.how_many_words)
            
        textes = [idea.idea.text[:9] for idea in ideas]

        s_tuple_how_many_words = sorted(enumerate(how_many_words), key=lambda x: x[1], reverse = True)
        
        s_how_many_words = [ t[1] for t in s_tuple_how_many_words]
        s_indexes = [ t[0] for t in s_tuple_how_many_words]
 
        nusers_reorganised_according_nwords_sort = [how_many_users[s_index] for s_index in s_indexes]
        
        set_how_many_words = set(how_many_words)      
        uniq_how_many_words = list(set_how_many_words)
    
        sorted_indexes = range(len(s_indexes))
     
#        sorted_indexes = range(len(s_indexes))
#        
#        logging.info('Idea/get_context_data/début du texte (avant le filtre) : %s' % textes)
#        logging.info('Idea/get_context_data/valeur de s_tuple_how_many_words : %s' % s_tuple_how_many_words)
#        logging.info('Idea/get_context_data/valeur de s_how_many_words : %s' % s_how_many_words)
#        logging.info('Idea/get_context_data/valeur de how_many_users : %s' % how_many_users)
#        logging.info('Idea/get_context_data/valeur de nusers_reorganised_according_nwords_sort : %s' % nusers_reorganised_according_nwords_sort)
#        logging.info('Idea/get_context_data/avant le tri, les textes sont rangés dans cet ordre : %s' % s_indexes)  
            
        if self.is_debugger_active_for_test():
            print "------------------------------------------------------------------------"
            print u"  CLASSE   : communications.IdeasContainer"
            print u"  FONCTION : sort_ideas_according_request"
            print u"" 
            print "------------------------------------------------------------------------"
            print u"  RAPPORT  : INITIALISATION"
            print u"    n°1         "
            print "------------------------------------------------------------------------"
            print u"             Tableau des nombres de mots trouvés pour chacune des réflexions"            
            print u"             indices : {0}".format(range(len(s_indexes)))
            print u"             valeurs nombre de mots : {0}".format(how_many_words)
            print u"             Tableau des nombres d'utilisateurs trouvés pour chacune des réflexions"  
            print u"             valeurs nombre d'utilisateurs : {0}".format(how_many_users)   
            print u""            
            
            print "------------------------------------------------------------------------"
            print u"  RAPPORT  : TRI DECROISSANT sur les occurences de mots"
            print u"    n°2        "
            print "------------------------------------------------------------------------"
            print u"             Tableau trié des nombres de mots calculés pour chacune des réflexions"            
            print u"               indices : {0}".format(s_indexes)
            print u"               valeurs nombre de mots : {0}".format(s_how_many_words)
            print u"             Tableau des nombres d'utilisateurs calculés pour chacune des réflexions"  
            print u"             trié selon l'ordre du tableau du nombre de mots (%s)" % s_indexes 
            print u"               valeurs nombre d'utilisateurs : {0}".format(nusers_reorganised_according_nwords_sort)
            

            print u""  
            print "------------------------------------------------------------------------"
            print u"  RAPPORT  : s'il ya des nombres de mots identiques entre plusieurs réflexions"
            print u"    n°3      TRI DECROISSANT sur le nombre d'utilisateurs"   
            print "------------------------------------------------------------------------"
            print u"             RAPPEL :"
            print u"               indices : {0}".format(s_indexes)
            print u"               valeurs nombre de mots : {0}".format(s_how_many_words)
            print u"               valeurs nombre d'utilisateurs :{0}".format(nusers_reorganised_according_nwords_sort)
            
            i_for_test = 1
            
        for uniq_how_many_word in uniq_how_many_words:
            # il s'agit de trier
            boolarray = [uniq_how_many_word == s_how_many_word for s_how_many_word in s_how_many_words] 
            extracted_how_many_users = [value for index,value in enumerate(nusers_reorganised_according_nwords_sort) if boolarray[index]]
            extracted_indexes_how_many_users = [index for index,value in enumerate(nusers_reorganised_according_nwords_sort) if boolarray[index]]
            s_extracted_how_many_words = [index for index,value in enumerate(s_how_many_words) if boolarray[index]]
    
            s_tuple_extracted_how_many_users = sorted(enumerate(extracted_how_many_users), key=lambda x: x[1], reverse = True)
            s_extracted_how_many_users = [ t[1] for t in s_tuple_extracted_how_many_users]     
            s_extracted_indexes_how_many_users = [ t[0] for t in s_tuple_extracted_how_many_users]  
            
            if self.is_debugger_active_for_test():
                
                print u"               "
                print u"               traitement n° {0} sur les nombres de mots".format(i_for_test)  
                print u"                  nombre en cours de traitement = {0}".format(uniq_how_many_word) 
                print u"                  True indique où se trouve le nombre {0} dans {1}: ".format(uniq_how_many_word, s_how_many_words)                
                print u"                  {0}".format(boolarray)
                if boolarray.count(True)== 1:
                    print u"                     1 seul indice à True => Pas de réarrangement(s) possible pour l'unique occurence du nombre {0} dans le tableau ".format(uniq_how_many_word) 
                    

            
            # Réarrangement de la partie du tableau du nombre de mots 
            # dont la valeur correspond à uniq_how_many_word
            # les réflexions appartenant à un utilisateur qui est mentionné
            # dans la recherche remonte  
            
            for i in range(len(s_extracted_indexes_how_many_users)):
                index = extracted_indexes_how_many_users[i]
                logging.info("Idea/get_context_data/position prête à être traitée dans le tableau du nombre d'utilisateurs (équivaut à tableau des mots): %s" % index)
                nouvelle_valeur = s_extracted_how_many_users[s_extracted_indexes_how_many_users[i]]
                ancien_indice = extracted_indexes_how_many_users[s_extracted_indexes_how_many_users[i]]
                logging.info("Idea/get_context_data/l'ancienne valeur du nombre d'utilisateur : {0} a été remplacée par nouvelle : {1}".format(s_extracted_how_many_users[i], nouvelle_valeur))
                logging.info("Idea/get_context_data/l'ancien indice dans la tableau du nombre de mots : {0} a été remplacé par le nouvel indice : {1}".format(ancien_indice, index))
                sorted_indexes[index] = s_indexes[ancien_indice]
 
            
            if self.is_debugger_active_for_test():
                
                if boolarray.count(True)> 1:
                    
                    print u"                     plusieurs indices à True => Réarrangement(s) possibles"
                    print u"                     pour les occurences du nombre {0} dans le tableau".format(uniq_how_many_word)
                    print u"                       si la réflexion est celle d'un utilisateur inscrit dans la recherche, elle doit être affichée en premier"
                    print u"                       le tableau qui le précise est {0}".format(nusers_reorganised_according_nwords_sort)    
                    print u"                       pour rappel en lui appliquant {0}".format(boolarray)                       
                    print u"                       on extrait le sous tableau qui nous intéresse :"                      
                    print u"                              dont les indices sont {0}".format(extracted_indexes_how_many_users)   
                    print u"                              dont les valeurs sont {0}".format(extracted_how_many_users)   
                    print u"                       le tri décroissant sur ce sous tableau donne :"                        
                    print u"                              les indices {0}".format(s_extracted_indexes_how_many_users)   
                    print u"                              les valeurs {0}".format(s_extracted_how_many_users)
                    print u"                       le nouvel ordre des réflexions devient :"  
                    print u"                           les valeurs {0}".format(sorted_indexes)    
           
            if self.is_debugger_active_for_test():        
                i_for_test +=1

        logging.info('Idea/get_context_data/après le tri, les textes sont rangés dans cet ordre : %s' % sorted_indexes)   
        
        return sorted_indexes
        
    def sort_ideas_according_request(self):
        
        # 1 / on exclut les réflexions de certains utilisateurs (self.exclude_users)
        
        if self.has_exclude_users:
            
            extracted_ideas = []
            
            for idea in self.data:
                for profile in idea.profiles:
                    if profile.user.username in self.exclude_users:
                        pass
                    else:
                        extracted_ideas.append(idea)
                        
            ideas = extracted_ideas


        # 2 / on ne garde que les réflexions de certains utilisateurs (self.include_users)

        if self.has_include_users:
            
            extracted_ideas = []
            
            for idea in ideas:
                for profile in idea.profiles:
                    if profile.user.username in self.include_users:
                        extracted_ideas.append(idea)
                        
            ideas = extracted_ideas


        # 3 / Réarrangement des réflexions restantes :
        # - sur le nombre d'occurences trouvées pour chaque mot 
        #   présent dans la requête de recherche
        #   TRI DECROISSANT

        if len(ideas) > 0:
            
            sorted_indexes = self.sort_ideas_according_number_of_words(ideas)
            sorted_ideas = [ideas[sorted_index] for sorted_index in sorted_indexes]
        else:
            sorted_ideas = []
            
        return sorted_ideas
        
    
    def get_context_data(self):
        
        if self.has_words or self.has_include_users or self.has_exclude_users:
            
            data = self.sort_ideas_according_request()
            
        else:
            data = self.data
            
        rcinfos = generic.get_returned_code_ok_object()
        return rcinfos, data

    
    def get_username_choices(self):
        usernames = [u"Toutes"]
        for data in self.data:
            curusernames = data.get_usernames()
            for curusername in curusernames:
                usernames.append(curusername)
        usernames = list(set(usernames))
        usernames = sorted(usernames)

        
        username_choices = []
        i = 0
        for username in usernames:
            username_choices.append((i, username))
            i+=1
        return username_choices
        
        
    def set_requested_action_index(self, index, requested_action_index):
        data = self.get_context_data()
            
        boolarray = [wdata.index == index for wdata in data]
        logging.info('IdeasFilterCom/set_requested_action_index/valeur de boolarray : %s' % boolarray)
        index = boolarray.index(True)
        
        data[index].set_requested_action_index(requested_action_index)
        