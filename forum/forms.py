#-*- coding: utf-8 -*-
from django import forms
from models import Idea, Profile, ReflexionState
from django.contrib.auth.models import User
import logging
import datetime
from django.utils.translation import ugettext_lazy as _
from taggit.models import Tag

# Create your forms here.

  
class CombinedFormBase(forms.Form):
    form_classes = []
    
    def __init__(self, *args, **kwargs):
        super(CombinedFormBase, self).__init__(*args, **kwargs)
        for f in self.form_classes:
            name = f.__name__.lower()
            setattr(self, name, f(*args, **kwargs))
            form = getattr(self, name)
            self.fields.update(form.fields)
            self.initial.update(form.initial)

    def is_valid(self):
        isValid = True
        for f in self.form_classes:
            name = f.__name__.lower()
            form = getattr(self, name)
            if not form.is_valid():
                isValid = False
        # is_valid will trigger clean method
        # so it should be called after all other forms is_valid are called
        # otherwise clean_data will be empty
        if not super(CombinedFormBase, self).is_valid() :
            isValid = False
        for f in self.form_classes:
            name = f.__name__.lower()
            form = getattr(self, name)
            self.errors.update(form.errors)
        return isValid

    def clean(self):
        cleaned_data = super(CombinedFormBase, self).clean()
#        for f in self.form_classes:
#            name = f.__name__.lower()
#            form = getattr(self, name)
#            logging.info('CombinedFormBase/clean/form : {0}'.format(type(form)))
#            cleaned_data.update(form.cleaned_data)
        return cleaned_data
        
#    def save2(self):
#        logging.info('CombinedFormBase/nombre de classe form : {0}'.format(len(self.form_classes)))
#        cleaned_data = self.clean()
#        for f in self.form_classes:
#            name = f.__name__.lower()
#            form = getattr(self, name)
#            success = form.add_exclude_fields()
#            form.save()
#        return True
        
#class ReflexionForm(forms.ModelForm):
#
#    
#    class Meta:
#        model = Reflexion
#        exclude = ('moderations','tags')
        
#class PublicationForm(forms.ModelForm):
#    
#    def __init__(self, in_new_discussion=False):
#        if in_new_discussion:
#            pass
#        else:
#            in_new_discussion = forms.BooleanField(label =u"à publier dans un nouveau fil de discussion", required = False)  
#                 
#    class Meta:
#        model = Reflexion
##        exclude = ('moderations','tags')
#        exclude = ('moderations',)
#        widgets = {'text':forms.Textarea(attrs={'cols': 100, 'rows': 10}),
#                   'tags': forms.Textarea(attrs={'cols': 100, 'rows': 1}),
#                   'new_discussion_asked': forms.CheckboxInput(attrs={'size': 20})
#                   }
#        labels = {'tags' : _(u"Mots-clés"),}
#        help_texts = {'tags': _(u"Liste servant à référencer vos textes. Séparez chaque mot-clé par une virgule"),
#        }
        
#class PublicationFormNewDiscussion(PublicationForm):
#    
#    def __init__(self, *args, **kwargs):
#        super(PublicationFormNewDiscussion, self).__init__(*args, **kwargs)

#class IdeaFilterForm(forms.Form):
#    
#    username = forms.ChoiceField(label =u"Personne")
##    tank = forms.IntegerField(widget=forms.HiddenInput()) 

class TagForm(forms.ModelForm):
    
#    tag = forms.CharField(label=u"Nom", 
#                          required = True,
#                          widget = forms.Textarea(attrs={'cols': 20, 'rows': 1}))
                                          
    class Meta:
        model = Tag
        labels = {'name' : _(u"Nom"),}
        exclude = ('slug',)
        
class IdeaFilterForm(forms.Form):
    
#        who = forms.CharField(label=u"Qui en a parlé ?", widget=forms.TextInput(attrs={'size' : 35}))
#        notwho = forms.CharField(label=u"Qui voulez-vous exclure de votre recherche ?", widget=forms.TextInput(attrs={'size' : 25}))
#        text = forms.CharField(label=u"Une idée de contenu ?", widget=forms.Textarea(attrs={'cols' : 55, 'rows':5}))        
        
        requested_users = forms.CharField(label=u"Qui en a parlé ?", 
                                          required = False,
                                          widget = forms.Textarea(attrs={'cols': 75, 'rows': 1}))
        excluded_users = forms.CharField(label=u"Qui voulez-vous exclure de votre recherche ?", 
                                         required = False,
                                         widget = forms.Textarea(attrs={'cols': 50, 'rows': 1}))
        text = forms.CharField(label=u"Une idée de contenu ?", 
                               required = False,
                               widget = forms.Textarea(attrs={'cols': 70, 'rows': 3})) 

        class Meta:
            widgets = {'text':forms.Textarea(attrs={'cols': 10, 'rows': 10}),
                       'requested_users': forms.Textarea(attrs={'cols': 100, 'rows': 1})}
        
class IdeaForm(forms.ModelForm):
                    
    class Meta:
        model = Idea
        exclude = ('moderations',)
        widgets = {'text':forms.Textarea(attrs={'cols': 100, 'rows': 10}),
                   'tags': forms.Textarea(attrs={'cols': 100, 'rows': 1})}
        labels = {'tags' : _(u"Mots-clés"),}
        help_texts = {'tags': _(u"Liste servant à référencer vos textes. Séparez chaque mot-clé par une virgule."),
        }
        
class PublicationForm(IdeaForm):
    
    in_new_discussion = forms.BooleanField(label =u"à publier dans un nouveau fil de discussion", required = False)  
                 
        

class UserForm(forms.ModelForm):
    
    class Meta:
        model = User
        exclude = ('groups', 
                   'user_permissions', 
                   'is_staff', 
                   'is_active', 
                   'is_superuser', 
                   'last_login', 
                   'date_joined')
        fields = ('username', 'password', 'first_name', 'last_name', 'email') 
        widgets = {'password':forms.PasswordInput(attrs={'cols': 80, 'rows': 20})}
        required = ('username', 'password')
        labels = {'username' : _("Nom d'utilisateur *"),
                  'password':_("Mot de passe *"),
        }
        
#    def add_exclude_fields(self):
#        self.groups = []
#        self.user_permissions = []
#        self.is_staff = False
#        self.is_active = True      
#        self.is_superuser = False
#        self.last_login = datetime.datetime.today()
#        self.date_joined = datetime.datetime.today()
#        return True
        
class ReflexionStateForm(forms.ModelForm):
    
        
    def __init__(self, *args, **kwargs):
        
        state_label=kwargs.pop('state_label')
        state_help_text=kwargs.pop('state_help_text')
            
        super(ReflexionStateForm, self).__init__(*args, **kwargs)

        self.fields['state'].label = state_label
        self.fields['state'].help_text = state_help_text
            
        
        
    class Meta:
        model = ReflexionState
        exclude = ('date', 'reflexion')
        widgets = {'state':forms.Select(attrs={'onchange': 'this.form.submit();'})}

        
        
        
#class ReflexionStateForm(forms.ModelForm):
#    
#        
#    def __init__(self, state_label, state_help_text, *args, **kwargs):
#        super(ReflexionStateForm, self).__init__(*args, **kwargs)
#        self.fields['state'].label = state_label
#        self.fields['state'].help_text = state_help_text
#    class Meta:
#        model = ReflexionState
#        exclude = ('date', 'reflexion')

        
class ProfileForm(forms.ModelForm):
    
    class Meta:
        model = Profile
        exclude = ('inscrit_newsletter', 'reflexion', 'signature', 'user')
        fields = ('avatar', 'site_web')

#    def add_exclude_fields(self):
#        self.inscrit_newsletter = []
#        self.is_logged = False
#        self.signature = [] 
#        self.user = []  
#        return True
        
class AccountCreationForm(CombinedFormBase):
    form_classes = [UserForm, ProfileForm]
    
#class PublicationForm(CombinedFormBase):
#    form_classes = [PublicationFormInNewDiscussion, CheckBoxInNewDiscussionForm]


        