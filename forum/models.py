#-*- coding: utf-8 -*-

###############################################################################
from django.db import transaction
import django.db as db
from django.db import models

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from django.contrib.auth.models import User

from django.contrib.auth.signals import user_logged_in, user_logged_out

from django.db.models.signals import post_save
#from django.core import validators
#from django.utils.translation import ugettext_lazy as _
import logging
import datetime

from taggit.managers import TaggableManager

import quadrantides.generics as generic

                            
###############################################################################
# Liste des classes du modèle
###############################################################################
	
class Moderation(models.Model):
    """
    Cette classe permet de définir ce qu'est une modération
    """
    
    auteur = models.ForeignKey(User, verbose_name="auteur")
    
    contents = (
         ('conforme_ethique', "conforme à l'éthique du site"),
         ('pas_conforme_propos_injurieux', "pas conforme: propos injurieux"),
         )
    
    content = models.CharField(max_length=100,
                               choices = contents,
                               default = 'conforme_ethique',
                               verbose_name="commentaire")
                               
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "Modération de {0} sur {1}".format(self.auteur, self.content_object)
        


        
class Profile(models.Model):
    """
    Cette classe permet de définir ce qu'est un profil utilisateur
    """
    
    user = models.OneToOneField(User)  # La liaison OneToOne vers le modèle User
    site_web = models.URLField(blank=True)
    avatar = models.ImageField(null=True, blank=True, upload_to="avatars/")
    signature = models.TextField(blank=True)
    inscrit_newsletter = models.BooleanField(default=False)
    is_logged = models.BooleanField(default=False)

 
    def __str__(self):
        return "Profil de {0}".format(self.user.username)
    


class Idea(models.Model):
    """
    Cette classe permet de définir ce qu'est une idéee
    """
    text = models.TextField(unique=True,
                            verbose_name="texte"
#                            help_text=_('Champ obligatoire. Il doit contenir des lettres, des chiffres et '
#                                        'seulement les caractères de ponctuation suivants : ". , ; : - ! ? %"'),
#                            validators=[
#                                validators.RegexValidator(r'^[\w.,;:-\!]+$', _('Veuillez saisir un simple texte.'), 'invalid')
#                                    ]
                            )
                                    
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date création")
    moderation_date = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name="date de la dernière modération")
    tags = TaggableManager()
    
    def __unicode__(self):
        return u"L'idee suivante existe dans la BD : %s" % self.text[0:50]
          
	
    def get_all_text(self):
        return self.text

    def get_short_text(self):
        
        if len(self.text) > 150:
            return self.text[:150] + '...'
        return self.text
        

class Thread(models.Model):
    """
    Cette classe permet de définir ce qu'est un fil de discussion 
    """
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date d'ouverture")
    is_opened = models.BooleanField(default=True, verbose_name="coché si la discussion est actuellement ouverte")
    def get_title(self):
        return u"Discussion n° {0}".format(self.id)
    def __unicode__(self):
        return u"Discussion n° {0}, ouverte le {1}".format(self.id, self.date)


class Discussion(models.Model):
    """
    Cette classe permet de définir ce qu'est une discussion 
    (association entre une publication et un fil de discussion)
    """  
    publication = models.ForeignKey('PublishedReflexion')
    thread = models.ForeignKey('Thread')
    publication_order = models.PositiveIntegerField(verbose_name='ordre de publication dans le fil de discussion')
    
    
class Reflexion(models.Model):
    """
    Cette classe permet de définir ce qu'est une réflexion 
    (association entre un Utilisateur et un Texte)
    """

    profile = models.ForeignKey(Profile)
    idea = models.ForeignKey(Idea)
    categories = TaggableManager()  
    
        
    def get_short_text(self):
        text = self.idea.get_short_text()
        return u"%s" % text
        
    def get_all_text(self):
        text = self.idea.get_all_text()
        return u"%s" % text
        
    def get_title(self):
        return u"Association de {0} avec l'idee : {1}".format(self.profile.user.username, self.get_short_text())
        
    def __str__(self):
        return self.get_title()
        
    def __unicode__(self):
        return u"%s" % self.get_title()
        
    def get_enumerated_categories(self):
        names = list(self.categories.names())
        if len(names) == 0:
            enumerated_names = u"aucun thème défini"
        else:
            enumerated_names = ''
            for index in range(len(names)): 
                if index < len(names)-1:
                    enumerated_names += names[index] + ', '
                else:
                    enumerated_names += names[index]
                   
        return enumerated_names
        
        
class ReflexionState(models.Model):
    """
    Cette classe permet de définir l'historique des états d'une Réflexion)
    """
    contents = (
         ('immature', "immature, invisible des autres Utilisateurs"),
         ('mature', "mature, visible des autres Utilisateurs"),
         ('caduque', "caduque, invisible des autres Utilisateurs"),
         )
         
    date = models.DateTimeField(auto_now_add=True, verbose_name=u"date de l'évolution")
    state = models.CharField(null=False, 
                             choices = contents,
                             default = 'mature',
                             max_length=70,
                             verbose_name=u"état de l'évolution")
    reflexion = models.ForeignKey('Reflexion')
    
    def get_current(self):
        return u"{0}".format(self.state)
    
    def __unicode__(self):
        return u"Reflexion : {0}, Date : {1}, Etat : {2}".format(self.reflexion, self.date, self.state)
        
class PublishedReflexion(models.Model):
    """
    Cette classe permet de définir ce qu'est une publication
    """
    
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date de publication")
#    reflexion = models.ForeignKey('Reflexion')
    reflexions = models.ManyToManyField("Reflexion", null=False, blank=False, verbose_name="Liste des reflexions")
    linked_posts = models.ManyToManyField("self", null=True, blank=True, verbose_name="sources, réponses")

        
    def get_first_reflexion(self):
        
        reflexions = list(self.reflexions.all())
        if reflexions:
            logging.info(u"get_first_reflexion / reflexions = %s" % reflexions[0])
            return reflexions[0]
        else: 
            return []
         
   
   
    def get_publication_profile(self):
        first_reflexion = self.get_first_reflexion()

        return first_reflexion.profile
        
    def get_publication_username(self):
        profile = self.get_publication_profile()
        return profile.user.username

        
    def get_first_reflexion_short_text(self):
    
        first_reflexion = self.get_first_reflexion()
        text = first_reflexion.idea.get_short_text()
        return u"%s" % text
        
    def get_reflexions(self):
        
        return list(self.reflexions.all())
        
    def get_tag_names(self):
        reflexions = self.get_reflexions()
        names = []
        for reflexion in reflexions: 
            curnames = reflexion.idea.tags.names()
            for curname in curnames:
                names.append(curname)
                   
        return names
        
    def get_category_names(self):
        reflexions = self.get_reflexions()
        names = []
        for reflexion in reflexions: 
            curnames = reflexion.categories.names()
            for curname in curnames:
                names.append(curname)
                   
        return names
        
    def get_title(self):
        
        nreflexions = self.reflexions.count()
        reflexion = self.get_first_reflexion()
        
        return u"Publication effectuee par {0}, le {1} : elle contient {2} idee(s)".format(reflexion.profile.user.username, self.date, nreflexions)
 

    def __str__(self):
        return self.get_title()
        
    def get_short_text(self):
#        reflexions = self.reflexions.all()
#        
#        text = reflexions[0].idea.get_short_text()
        return u"%s" % self.get_first_reflexion_short_text()
        
    def get_all_text(self):
        reflexions = self.get_reflexions()
        n = len(reflexions)
        text = ""
        i = 0
        for reflexion in reflexions:    
            text = text + reflexion.idea.get_all_text() 
            if i > 0 & i < n:
                text = text + u"\n"
            i += 1
        return u"%s" % text
        
    def __unicode__(self):
        return u"%s" % self.get_title()
        
    def has_been_published_less_than_n_days_from_today(self, n):
        """ Retourne True si la réflexion a été publiée dans
            les n derniers jours """
        return (datetime.now() - self.date).days < n
        
        
#class Adhesion(models.Model):
#    """
#    Cette classe permet de définir ce qu'est une adhésion
#    """
#    
#    date_movement = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date du mouvement")
#    is_activated = models.BooleanField(default=True, verbose_name="coché si l'adhésion est actuellement en cours")
#    profile = models.ForeignKey(Profile)
#    reflexion = models.ForeignKey('Reflexion')
#    
#    def get_title(self):
#        if self.is_activated:
#            movement = u"adhère"
#        else:
#            movement = u"n'adhère plus" 
#        return u"{0} {1}, depuis le {2}, à la réflexion : {3}".format(self.profile.user.username, movement, self.date_movement, self.reflexion.get_reflexion_short_text())
#        
#    def __str__(self):
#        
#        return u"%s" % self.get_title()
#        
#    def __unicode__(self):
#        return u"%s" % self.get_title()
        
#class Subjectivity(models.Model):
#    """
#    Cette classe définit l'espace de subjectivité de l'Utilisateur
#    """
#    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="date de création")
#    profile = models.ForeignKey(Profile)
#    reflexion = models.ForeignKey('Reflexion')
#    reflexion_order = models.PositiveIntegerField(verbose_name="ordre d'affichage")
#    categories = TaggableManager()  
#    
#    
#    def get_title(self):
#
#        return u"La réflexion suivante : {0} doit être affichée en position {1}, dans l'espace de subjectivité de l'Utilisateur : {2}".format(self.reflexion.get_reflexion_short_text(), self.reflexion_order, self.profile.user.username)
#        
#    def __str__(self):
#        
#        return u"%s" % self.get_title()
#        
#    def __unicode__(self):
#        return u"%s" % self.get_title()
#    
        
###############################################################################
# Liste des fonctions qui traitent les signaux définis au-dessous
###############################################################################
def is_logged_out(sender, request, user, **kwargs):

    if user: # user not None
        user.profile.is_logged=False
        user.profile.save()
    
def is_logged_in(sender, request, user, **kwargs):


    if user:# user not None
    
        user.profile.is_logged=True
        user.profile.save()
    
def create_profile_from_post_save_signal(sender, instance, created, **kwargs):
    if created:
        profile, created = Profile.objects.get_or_create(user=instance)


def create_reflexionstate_from_post_save_signal(sender, instance, created, **kwargs):
    if created:
        state = "immature"
        reflexionstate, created = accessor_create_reflexionstate(instance, state)
        
###############################################################################
# Liste des signaux
###############################################################################
    
user_logged_in.connect(is_logged_in)   
user_logged_out.connect(is_logged_out)
post_save.connect(create_profile_from_post_save_signal, sender=User)
post_save.connect(create_reflexionstate_from_post_save_signal, sender= Reflexion)
###############################################################################

###############################################################################
# Liste des accesseurs 
#                                                   fonctions getters / setters
###############################################################################
def accessor_create_user(**kwargs):
    
    name = u"accessor_create_user"
    user = User(**kwargs)
    rcinfos = generic.manage_save_exception(name, user)
    
    return rcinfos, user
        
def accessor_get_users():
    name = u"accessor_get_users"

    try:
        users = User.objects.all().order_by("username")
    except User.DoesNotExist as e:
        success = False
        code = e.__class__.__name__
        message = e.message
#    except User.MultipleObjectsReturned:
#        success = False
#        code = e.__class__.__name__
#        message = e.message     
    else:
        users = list(users)
        success, code, message = generic.returned_code_ok()
    
    if success:
        pass
    else:
        users = []

    return generic.ReturnedCode(name, success, code, message), users    
    
def accessor_get_profile(username):
    
    name = u"accessor_get_profile"

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except User.MultipleObjectsReturned:
        success = False
        code = e.__class__.__name__
        message = e.message     
    else:
        profile = user.profile
        success, code, message = generic.returned_code_ok()
    
    if success:
        pass
    else:
        profile = []

    return generic.ReturnedCode(name, success, code, message), profile
    
###############################################################################       
def accessor_create_idea(text, **kwargs):
    
    name = u"accessor_create_idea"
    
    try:
        oqs = Idea.objects.filter(text = text)
    except Idea.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message     
        creation_requested = True
    else:
        if oqs:
            idea = oqs.get()
#            idea = generic.returned_var_as_list(idea)
            creation_requested = False
        else:
            creation_requested = True
    
    
    if creation_requested:
        
        idea = Idea(text = text)
        rcinfos = generic.manage_save_exception(name, idea)
        
        if rcinfos.success:
            if kwargs:
                if 'tags' in kwargs:
                    tags = kwargs['tags']
                    for tag in tags:
                        if tag not in str(idea.tags.names()):
                            idea.tags.add(tag)
        
        
    else: 
        success, code, message = generic.returned_code_ok_already_in_db()
        rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, idea

###############################################################################       
def accessor_get_idea(text):
    
    name = u"accessor_get_idea"
    
    try:
        oqs = Idea.objects.filter(text = text)
    except Idea.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        idea = []
    else:
        if oqs:
            idea = oqs.get()
        else:
            idea = []
    
    
    if idea:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, idea
    
###############################################################################       
def accessor_get_ideas():
    
    name = u"accessor_get_ideas"

    try:
        qs = Idea.objects.all()
        
    except Idea.DoesNotExist as e:
        success = True
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        rcinfos = generic.get_returned_code_ok_object()
        success = rcinfos.success

    if success:
        ideas = list(qs)
    else: 
        ideas = []
        rcinfos = generic.ReturnedCode(name, success, code, message)
        
  
    return rcinfos, ideas
    
    
###############################################################################
def accessor_create_thread():
    
    name = u"accessor_create_thread"
    
    thread = Thread()
    rcinfos = generic.manage_save_exception(name, thread)
    return rcinfos, thread

###############################################################################       
def accessor_get_reflexions(idea):
    
    name = u"accessor_get_reflexions"
    try:
        qs = Reflexion.objects.filter(idea = idea)
        
    except Reflexion.DoesNotExist as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        success, code, message = generic.returned_code_ok()

    if success:
        reflexions = list(qs)
    else:
        reflexions = []
        
    rcinfos = generic.ReturnedCode(name, success, code, message)
           
    return rcinfos, reflexions
    
###############################################################################       
def accessor_get_reflexion(idea, profile):
    
    name = u"accessor_get_reflexion"
    try:
        oqs = Reflexion.objects.filter(idea = idea, profile = profile)
    except Reflexion.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        reflexion = []
    else:
        if oqs:
            reflexion = oqs.get()
        else:
            reflexion = []
    
    if reflexion:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, reflexion
    
###############################################################################
def accessor_create_or_update_reflexion(name, idea, profile, **kwargs):  
    
    # La clé primaire est l'ID de autoField
    # Django ne permet pas que ce soit la paire (profile, idea)
    # conséquence : il ne s'agit de stocker qu'une seule association
    #               il est donc nécessaire de tester l'existence
    #               avant de stocker en base de données
    # attention : on ne peut pas utiliser la méthode get_or_create car
    #             un signal post_save est intercepté pour créer automatiquement
    #             un enregistrement dans la table ReflexionState : la méthode 
    #             save est incontournable
    
    try:
        reflexion = Reflexion.objects.get(idea = idea, profile = profile)
        
    except Reflexion.DoesNotExist:
        
        # l'association n'existe pas encore : il faut la créer
        reflexion = Reflexion(idea = idea, profile = profile)
        
        rcinfos = generic.manage_save_exception(name, reflexion)
        
    else:
        
        # l'association existe déjà
        success, code, message = generic.returned_code_ok_already_in_db()
        rcinfos = generic.ReturnedCode(name, success, code, message)
        
    if rcinfos.success:
        # si la réflextion a des catégories, il faut les stocker   
        if kwargs:
            if 'categories' in kwargs:
                categories = kwargs['categories'] 
                for category in categories:
                    if category not in reflexion.categories.names():
                        reflexion.categories.add(category)
                    
                
    return rcinfos, reflexion
###############################################################################
def accessor_create_reflexion(idea, profile, **kwargs):  
    name = u"accessor_create_reflexion"
           
    return accessor_create_or_update_reflexion(name, idea, profile, **kwargs)

###############################################################################
def accessor_update_reflexion(idea, profile, **kwargs):  
    name = u"accessor_update_reflexion"
           
    return accessor_create_or_update_reflexion(name, idea, profile, **kwargs)
###############################################################################    
def accessor_create_reflexionstate(reflexion, state):
    name = u"accessor_create_reflexionstate"
    reflexionstate = ReflexionState(reflexion = reflexion)
    reflexionstate.state = state
    rcinfos = generic.manage_save_exception(name, reflexionstate)
    return rcinfos, reflexionstate

###############################################################################       
def accessor_get_reflexionstate(reflexion):
    
    name = u"accessor_get_reflexionstate"
    
    try:
        oqs = ReflexionState.objects.filter(reflexion = reflexion)
    except ReflexionState.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        reflexion = []
    else:
        if oqs:
            reflexionstate = oqs.get()
            reflexionstate = generic.returned_var_as_list(reflexionstate)
        else:
            reflexionstate = []
    
    if reflexionstate:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, reflexionstate
    
###############################################################################
def accessor_create_publication(reflexions, **kwargs):  
    name = u"accessor_create_publication"
    publication = PublishedReflexion()
    
    logging.info('accessor_create_publication/type(reflexions) : %s' % reflexions)
            
    if isinstance(reflexions, list):
        pass
    else:
        reflexions = generic.returned_var_as_list(reflexions)
  
    rcinfos = generic.manage_save_exception(name, publication)
    
    logging.info('accessor_create_publication/rcinfos.success : %s' % rcinfos.success)

    logging.info('accessor_create_publication/type(publication) : %s' % type(publication))
        
    if rcinfos.success:
                      
                     
        publication.reflexions.add(*reflexions)

#        rcinfos = generic.manage_save_exception(name, publication)
        
        logging.info('accessor_create_publication/rcinfos.success : %s' % rcinfos.success)
        logging.info('accessor_create_publication/publication : %s' % publication)
        
        if rcinfos.success:
        # si la publication a une source, il faut la stocker   
        
        
            if kwargs:
                if 'source' in kwargs:
                    source = kwargs['source']
                    publication.linked_posts.add(source)

    
    return rcinfos, publication

###############################################################################       
def accessor_get_all_published_reflexions():

    name = u"accessor_get_all_published_reflexions"
    
    try:
        oqs = PublishedReflexion.objects.all().order_by('-date')
    except PublishedReflexion.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        publishedreflexion = []
    else:
        if oqs:
            publishedreflexion = list(oqs.all())
            publishedreflexion = generic.returned_var_as_list(publishedreflexion)
        else:
            publishedreflexion = []
    
    if publishedreflexion:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, publishedreflexion
    
############################################################################### 
def accessor_get_discussions_for_requested_publication(publication):

    name = u"accessor_get_all_discussions"
    
    try:
        oqs = Discussion.objects.filter(publication=publication)
    except Discussion.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        discussions = []
    else:
        if oqs:
            discussions = list(oqs.all())
            discussions = generic.returned_var_as_list(discussions)
                
        else:
            discussions = []
    
    if discussions:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, discussions
###############################################################################  
def accessor_get_all_discussions():

    name = u"accessor_get_all_discussions"
    
    try:
        oqs = Discussion.objects.all()
    except Discussion.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        discussions = []
    else:
        if oqs:
            discussions = list(oqs.all())
            discussions = generic.returned_var_as_list(discussions)
                
        else:
            discussions = []
    
    if discussions:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, discussions
    
###############################################################################      
def accessor_get_username_published_reflexions(username):

    name = u"accessor_get_username_published_reflexions"
    rcinfos, profile = accessor_get_profile(username=username)
    if rcinfos.success:
        try:
            oqs = PublishedReflexion.objects.filter(profile = profile).order_by('-date')
        except PublishedReflexion.DoesNotExist as e:
            code = e.__class__.__name__
            message = e.message 
            publishedreflexion = []
        else:
            if oqs:
                publishedreflexion = list(oqs.all())
                publishedreflexion = generic.returned_var_as_list(publishedreflexion)
            else:
                publishedreflexion = []
                
    else:
        publishedreflexion = []
    
    if publishedreflexion:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, publishedreflexion
    
###############################################################################       
def accessor_get_newly_published_reflexions(days):

    name = u"accessor_get_newly_published_reflexions"
    
    try:
        oqs = PublishedReflexion.objects.filter(date__gt = datetime.datetime.today() -datetime.timedelta(days=7)).order_by('-date')
    except PublishedReflexion.DoesNotExist as e:
        code = e.__class__.__name__
        message = e.message 
        publishedreflexion = []
    else:
        if oqs:
            publishedreflexion = list(oqs.all())
            publishedreflexion = generic.returned_var_as_list(publishedreflexion)
        else:
            publishedreflexion = []
    
    if publishedreflexion:
        success, code, message = generic.returned_code_ok_already_in_db()
    else:
        success, code, message = generic.returned_code_ko_not_in_db()


    rcinfos = generic.ReturnedCode(name, success, code, message)


    return rcinfos, publishedreflexion
     
###############################################################################
#@transaction.atomic()
def accessor_create_discussion(publication, thread, publication_order, **kwargs):
    name = u"accessor_create_discussion"
    
    discussion = Discussion(publication = publication,
                            thread  = thread,
                            publication_order = publication_order)
                                     
    rcinfos = generic.manage_save_exception(name, discussion)
    if rcinfos.success:
        if kwargs:
            if 'integrity_error_simulated_failure' in kwargs:
                integrity_error_simulated_failure = kwargs['integrity_error_simulated_failure']              
                if integrity_error_simulated_failure:
                    raise db.IntegrityError
        
    return rcinfos, discussion

############################################################################### 
def accessor_get_profile_reflexion_categories(profile):
    name = u"accessor_get_profile_reflexion_categories"
    
    try:
        reflexions = Reflexion.objects.filter(profile=profile)
        
    except Reflexion.DoesNotExist as e:
        success = True
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        success, code, message = generic.returned_code_ok()
        categories = []
        for reflexion in reflexions:    
            l = list(reflexion.categories.all())
            for elt in l: 
                categories.append(elt)
                logging.info('get_all_categories/type pour categories = %s' % type(categories)) 
            
        
    rcinfos = generic.ReturnedCode(name, success, code, message)
     
    return rcinfos, categories
    
###############################################################################  
def accessor_get_latest_reflexion_state(reflexion):
    
    name = u"accessor_get_latest_reflexion_state"
    
    
    try:
        states = ReflexionState.objects.filter(reflexion = reflexion).order_by('-date')
        
    except ReflexionState.DoesNotExist as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        success, code, message = generic.returned_code_ok()

    if success:
        state = states[0] 
    else:
        state = []
        
    rcinfos = generic.ReturnedCode(name, success, code, message)
           
    return rcinfos, state
    
    
###############################################################################     
def accessor_get_latest_category_slug_reflexion_states(profile, slug):
    
    name = u"accessor_get_latest_reflexion_state" 
    
    try:
        reflexions = Reflexion.objects.filter(profile = profile, categories__slug=slug).order_by('id')  
        
    except Reflexion.DoesNotExist as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        current_reflexions = []
        if reflexions:
            for reflexion in reflexions:
                rcinfos, latest_reflexion_state = accessor_get_latest_reflexion_state(reflexion)
                success = rcinfos.success
                if success:
                    current_reflexions.append(latest_reflexion_state) 
                else:   
                    break
        else:
            success = False
            rcinfos = generic.returned_code_ko_not_in_db()

    if success:
        pass
    else: 
        current_reflexions = []
        if rcinfos:
            pass
        else:
            rcinfos = generic.ReturnedCode(name, success, code, message)
    
    return rcinfos, current_reflexions
    
###############################################################################
def accessor_get_latest_profile_reflexion_states(profile):
    
    
    name = u"accessor_get_latest_profile_reflexion_states" 
    
    try:
        reflexions = Reflexion.objects.filter(profile = profile).order_by('id')  
        
    except Reflexion.DoesNotExist as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:

        current_reflexions = []
        if reflexions:
            for reflexion in reflexions:
                rcinfos, latest_reflexion_state = accessor_get_latest_reflexion_state(reflexion)
                success = rcinfos.success
                if success:
                    current_reflexions.append(latest_reflexion_state) 
                else:   
                    break
        else:
            success = False
            rcinfos = generic.returned_code_ko_not_in_db()

            
    if success:
        pass
    else: 
        current_reflexions = []
        if rcinfos:
            pass
        else:
            rcinfos = generic.ReturnedCode(name, success, code, message)
            
    return rcinfos, current_reflexions
    
###############################################################################   
def accessor_extract_profiles_from_opened_threads():

    name = u"accessor_extract_profiles_from_opened_threads" 
    
    try:
        discussions = Discussion.objects.all()
        
    except Discussion.DoesNotExist as e:
        success = True
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        rcinfos = generic.get_returned_code_ok_object()
        success = rcinfos.success
        usernames = []
        profiles = []
        for discussion in discussions:
            
            if discussion.thread.is_opened:
                
                first_reflexion = discussion.publication.get_first_reflexion()
          
                discussion_username = first_reflexion.profile.user.username
    
                if discussion_username not in usernames:
                    if discussion_username == u'anonymous':
                        pass
                    else:
                        usernames.append(discussion_username)
                        profiles.append(first_reflexion.profile)
                        
                        
           
    if success:
        pass
    else: 
        profiles = []
        rcinfos = generic.ReturnedCode(name, success, code, message)

    
    return rcinfos, profiles
    
###############################################################################    
def accessor_extract_profiles_from_opened_thread(thread):
    
    name = u"accessor_extract_profiles_from_thread" 
    
    try:
        discussions = Discussion.objects.filter(thread=thread).order_by('publication_order')
        
    except Discussion.DoesNotExist as e:
        success = True
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        rcinfos = generic.get_returned_code_ok_object()
        success = rcinfos.success
        usernames = []
        profiles = []
        for discussion in discussions:
            
            if discussion.thread.is_opened:
                
                first_reflexion = discussion.publication.get_first_reflexion()
          
                discussion_username = first_reflexion.profile.user.username
    
                if discussion_username not in usernames:
                    if discussion_username == u'anonymous':
                        pass
                    else:
                        usernames.append(discussion_username)
                        profiles.append(first_reflexion.profile)
                        
#        usernames = []
#        profiles = []
#        
#        for discussion in discussions:
#            first_reflexion = discussion.publication.get_first_reflexion()
#            postusername = first_reflexion.profile.user.username.encode('utf8', 'replace')
#            logging.info('get_all_participants_in_opened_thread/cuurent username is %s ' % postusername)
#            if postusername not in usernames:
#                usernames.append(postusername)
#                profiles.append(first_reflexion.profile)
            
    if success:
        pass
    else: 
        profiles = []
        rcinfos = generic.ReturnedCode(name, success, code, message)
        
  
    return rcinfos, usernames, profiles
    
###############################################################################    
def accessor_get_discussions_from_opened_thread(thread):
    
    name = u"accessor_get_discussions_from_opened_thread" 
    
    try:
        discussions = Discussion.objects.filter(thread=thread).order_by('publication_order')
        
    except Discussion.DoesNotExist as e:
        success = True
        code = e.__class__.__name__
        message = e.message
    except db.IntegrityError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.DataError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    except db.Error as e:
        success = False
        code = e.__class__.__name__
        message = e.message         
    except db.DatabaseError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.OperationalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InterfaceError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.InternalError as e:
        success = False
        code = e.__class__.__name__
        message = e.message           
    except db.ProgrammingError as e:
        success = False
        code = e.__class__.__name__
        message = e.message   
    except db.NotSupportedError as e:
        success = False
        code = e.__class__.__name__
        message = e.message
    else:
        rcinfos = generic.get_returned_code_ok_object()
        success = rcinfos.success

    if success:
        pass
    else: 
        discussions = []
        rcinfos = generic.ReturnedCode(name, success, code, message)
        
  
    return rcinfos, discussions
    
###############################################################################     
#def accessor_get_profile_reflexions_by_category(profile, slug):
#    
#    name = u"accessor_get_profile_reflexions_by_category"
#    
#    try:
#        reflexions = Reflexion.objects.filter(profile = profile, categories__slug=slug)#.order_by('reflexion_order')
#        
#    except Reflexion.DoesNotExist as e:
#        success = True
#        code = e.__class__.__name__
#        message = e.message
#    except db.IntegrityError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message
#    except db.Error as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message
#    except db.DataError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message
#    except db.Error as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message         
#    except db.DatabaseError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message   
#    except db.OperationalError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message   
#    except db.InterfaceError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message   
#    except db.InternalError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message           
#    except db.ProgrammingError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message   
#    except db.NotSupportedError as e:
#        success = False
#        code = e.__class__.__name__
#        message = e.message
#    else:
#        success, code, message = generic.returned_code_ok()
#        
#        
#        index = 0
#        nelts = len(reflexions)
#        current_reflexions = []
#        while index < nelts:
#            current_reflexions.append(reflexions[index])
#            currentid = reflexions[index].id
#            while (index < nelts):
#                if reflexions[index].id == currentid:
#                    index += 1
#        # rupture
#        
#    rcinfos = generic.ReturnedCode(name, success, code, message)
#    
#    return rcinfos, current_reflexions