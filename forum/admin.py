# -*- coding: utf-8 -*-
from django.contrib import admin

# Register your models here.
from forum.models import Idea, PublishedReflexion, Moderation, Profile
from forum.models import ReflexionState, Reflexion
from forum.models import Thread, Discussion


class ProfileAdmin(admin.ModelAdmin):
    list_display   = ('avatar')

    
class ModerationAdmin(admin.ModelAdmin):
    list_display   = ('content', 'auteur', 'date_creaton')
    list_filter    = ('auteur',)
    date_hierarchy = 'date_ouverture'
    ordering       = ('date_ouverture', )
    search_fields  = ('content',)
    
class IdeaAdmin(admin.ModelAdmin):
    list_display   = ('short_text', 'date', 'moderation_date')
    list_filter    = ('moderation_date',)
    date_hierarchy = 'moderation_date'
    ordering       = ('moderation_date', )
    search_fields  = ('text',)
	
    def short_text(self, idea):
    
        text = idea.text
        if len(text) > 300:
            return '%s...Lire la suite' % text[0:299]
        else:
            return text

    short_text.short_description = u"Aperçu de l'idée"
	
class ReflexionStateAdmin(admin.ModelAdmin):
    list_display   = ('short_text', 'date','profile')
    list_filter    = ('date',)
    date_hierarchy = 'date'
	
    ordering       = ('date', )
	
    def short_text(self, reflexion_state):
    
        text = reflexion_state.reflexion.idea.text
        if len(text) > 300:
            return '%s...Lire la suite' % text[0:299]
        else:
            return text

    short_text.short_description = u"Aperçu de l'idée"
    
    def profile(self, reflexion_state):
    
        return reflexion_state.reflexion.profile

    profile.short_description = u"Utilisateur"
    
  
class PublishedReflexionAdmin(admin.ModelAdmin):
    list_display   = ('short_text', 'date','profile')
    list_filter    = ('date',)
    date_hierarchy = 'date'
	
    ordering       = ('date', )
	
    def short_text(self, published_reflexion):
         
        text = published_reflexion.get_all_text()
        if len(text) > 300:
            return '%s...Lire la suite' % text[0:299]
        else:
            return text

    short_text.short_description = u"Aperçu de l'idée"

    def profile(self, published_reflexion):
    
        return published_reflexion.get_publication_profile()

    profile.short_description = u"Utilisateur"
    
    
admin.site.register(Idea, IdeaAdmin)
admin.site.register(ReflexionState, ReflexionStateAdmin)
admin.site.register(PublishedReflexion, PublishedReflexionAdmin)
admin.site.register(Moderation)
admin.site.register(Profile)
admin.site.register(Thread)
admin.site.register(Discussion)
admin.site.register(Reflexion)


