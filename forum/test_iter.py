# -*- coding: utf-8 -*-
"""
Created on Thu Nov 06 21:01:41 2014

@author: Patrice
"""

class PostContext(object):
    
    def __init__(self, var):
        self.var = var


def test_iter():
    a = iter([PostContext(1), PostContext(2), PostContext(3)])
    print(a.next())
    print(a.next())
    
if __name__ == '__main__':
    test_iter()